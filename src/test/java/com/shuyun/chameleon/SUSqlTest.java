package com.shuyun.chameleon;

import com.shuyun.chameleon.parser.SUSQLAnalyser;
import com.shuyun.chameleon.parser.SUSQLHandler;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 上午10:48
 */

public class SUSqlTest {

    private static Logger LOG	= Logger.getLogger(SUSqlTest.class);

//    private Map<String, File> inputFileMap;
//    private Map<String, File>	expectedFileMap;
    private SUSQLAnalyser sqlAnalyser;

    @Before
    public void setUp() throws Exception {
        sqlAnalyser = new SUSQLAnalyser();
    }

    private void processQuery(final String query) {
        ParseTree tree = SUSQLHandler.getParseTree(query);
        if (null != tree) {
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(sqlAnalyser, tree);
        }
    }

    @Test
    @Ignore
    public void test() {

        String query = "SELECT ccc : tt FROM Contact";
        processQuery(query);
        String actual = sqlAnalyser.getSUSQLConstruct().binding().format().toSql();
//        assertThat(actual, is(query));
        System.out.println(actual);
    }

}
