/*
  1. i18n
    date-time
    currency
    label_translation

  2. data permissions

  data types:
        integer, number, string, datetime, date, time interval,
        URL, phone number, email address, currency, location, ID, reference, encrypted string, address
        object,
        There are case sensitive string fields and Case insensitive string fields

   How about the tree structure of data? How to query?   Just as ID relationships.
        How about introducing the concept of data category?   NO.
*/

/*
  1. shall we support the alias ??????
  2. How to express the polymorphism of the object itself??????
  3  Semi-join & Anti-join, should we require that the two data types are linked?
  4. The fact is here; two data types were related together, one direction or two directions, just an expressive wise matter.
  5. Universal IDs? Automatically generated Universal IDs?
  6. ID anti-join / semi-join apply to arbitrary datatype IDs
  7. references/data-link independent of data type? one-way or two-way?
     The different users have different scope, could the same link be created by different users?
     Should the reference /link have permission control?
  8. JSON-path as description of the fields

  Decisions:
  1. Only One level of aggregate,
  2. semi-join / anti-join must be in the linked data
  3.
  the problem of projection is not yet solved.

*/
/**
* <div style="display: inline-block; text-align: left ; width: 100%">
* This is the SUQL Official Grammar Definitions.
* <p>
* SUQL is a universal data query language for Shuyun CMP managed data. Its grammar was inspired by SQL, SOQL, GraphQL & Jsoniq.
* <br>
* The design principles were as follows:
* <ul>
* <li>  It is intended to be expressiveness yet easy to understand.
* <li>  It is used to query object types that were defined in CMP.
* <li>  DQL only, no DDL and DML, at least now.
* <li>  Support data polymorphism, aka, support data type generalization.
* <li>  No arbitrary join, only subquery with semi-join and anti-join on pre-defined data reference.
* <li>  Support Nested data object, such as JSON format data.
* <li>  It is intended to be with familiarity, just like SQL.
* <li>  Rich embedded scalar types, which would be with Biz meanings.
* <li>  Designed with i18n consideration in mind, at the first place.
* <li>  Designed with data permissions, which means you should rely on the query for the access control.
* </ul>
* <p>
* Data types in CMP have two tiers of meanings:
* <ul>
* <li>  It is a schema enforcement rule, with which all data in this type would conform
* <li>  It was a container, that all data in this type would be in this container, and queries happen on it.
* </ul>
* <p>
* There are two categories of data types, scalar types and complex types.
* <br>
* Scalar types were the most fundamental types, of which all complex types were composed. The scalar types were listed as follows:
* <ul>
* <li>  Integer : an Integer scalar type represents a signed 64-bit numeric non-fractional value.
* <li>  Number: a Number scalar type represents a signed double precision fractional value as specified by IEEE 754.
* <li>  String : a String scalar type represents textual data, represented as UTF-8 character sequence.
* <li>  Boolean : a Boolean type represents TRUE or FALSE
* <li>  ID: an ID represents a unique identifier, it was used to identify an object.
* <li>  Lable: a Label is a UTF-8 character sequence, just like a string, but it could be translated into difference languages for display on UI.
* <li>  Phone_No: a Phone number is a character sequence to represent the phone numbers.It has a maximum length of 18.
* <li>  Email: an Emails address is a character sequence to represent email addresses, which were defined in RFC 5322.
* <li>  Currency: a Currency represents a monetary value, which contains a currency type, and a numeric value (e.g. USD 23.32), which could be retrieved by functions respectively. Also it could be converted from one currency to another.
* <li>  Currency_Type: a Currency Type is a particular kind of Enumeration, whose value should be a Currency type which could be displayed as three character string as defined in ISO 4217, e.g. USD, CNY, GBP, EUR, etc..
* <li>  Time: a Time type represents specific Time point in history, now, or future, which could be represented in ISO 8601 format, e.g. 2010-12-01T10:01:20.000Z. It shouldn't before 1900 C.E. and after 2200 C.E.
* <li>  Time_Range: a Time Range type represents a time interval, it does specify the length of time, and doesn't specify when to begin or end. SUQL has embedded various Time_Range literal.
* <li>  TZ: a TZ type represents a time zone. a time zone could be specified by a TZ name abbreviation (as in IANA TZ DB  https://www.iana.org/time-zones ) or an ISO 8601 UTC offset.
* <li>  URL: a URL is a URL ^_~, it should be a URL, have to be a URL, and must be a URL, or there will be an error.
* <li>  ENUM: an ENUM is a value should be chosen from an options list. And every Option list could be treated as a label which means it supports i18n translation.
* </ul>
* <p>
* Complex types were composed of Scale types, among all complex types, Object, Interface and Union types could be queried by SUQL. The Complex types were listed as follows:
* <ul>
* <li>  Object type: an Object type is a key-value pair list, which key must be a string and which value could be of any Scalar Types,
*             an Array, another key-value list, or a reference to a piece of Data of another Object Type. the sorting order of key-value pair is not guaranteed.
*             There could be inheritance and generalization relationship between Object Types.
* <li>  Array type: an Array Type is a List of values of the same type, the sorting order of the elements were not guaranteed.
* <li>  Interface type: an Interface type is the generalization of an object type. Like an object type, it is defined as a key-value pair list and could be inherited by derived Object types.
*             However not like object type it could not be instantiated. This would be further explained later.
* <li>  Union type: a Union Type is a union of two or more Object Types, Interface types or other Union types. it could be query by SUQL polymorphically with "type of" expressions
* <li>  Tree type: a Tree type is a particular kind of Object type which could be queried as an Object type, and further it could be filtered with tree relationship criteria, e.g. parent_of, ancester_of, etc..
* </ul>
* <p>
* There are two kinds of relationships between Objects and Interfaces insofar.
* <ul>
* <li>  Generalization: an Object type or an Interface type could be a generalization of other object types. An Interface could not be generalized.
*             All data of a derived type must confirm the schema definition of the generalized type, which means they must be of the generalized type.
*             In concrete, the generalized types should have fewer fields (aka. key, we may use it interchangeably later) definition, which should be a subset of the derived type.
*             SUQL queries could happen on generalized types, and in this case, all instances of this type and its subtypes were in the container of query target.
*             If this generalized type is an Object type, there may be data instantiated from this type, and the query target container consists of these data and all data of its derived types;
*             otherwise, this is an interface type, from which there's no instantiated data, and the query target container consists of all data of its derived types.
* <li>  Reference: an Object/interface type could have one or more fields which value could be a reference to another object of some other object type, interface type or union type.
*             The element of an array could also be a reference to another type. There would be a soft constraint that the field name (aka key) to the reference value must be the type name of the target.
*             In SUQL query (this they are in the same realm, it would be the direct Type name or the full qualified name),
*             this was because the data modeling tool of CMP did it intentionally.
*             Reference Target Object (Referee) could be retrieved directly from Referer in SUQL, just like Object value of a field, and Referer Object could be queried by subquery from Referees.
* </ul>
* <p>
* The inheritance rules of object types and interface types are as follows
* <ul>
* <li> All the fields of the super type(generalized type) would be inherited by the subtype (derived type).
* <li> Inheritance creates an is-a relationship, which means a subtype data object would also be a data object of the supertype.
        <br>Therefore, when a super type is queried, all its subtype is also in the query target.
* <li> An object type could be an inheritance of one or more object types, interface types or both, while an interface type could only inherit from interface types.
* <li> If two supertypes of a subtype have the same fields (same name and same type), the subtypes will treat these two as one field, aka. the subtype would have only one such field.
* <li> If two supertypes of a subtype have fields with the same name but different type, such inheritance relationship couldn't be created, the UI or API would generate an error to respond the type definition request.
* <li> When a super type is queried, an object's actual type could be inferred by polymorphism mechanisms.
* </ul>
* <p>
* Object Type were identified by names, aka Object Type Name. An Object Name is within a Realm. A realm is represented by dot(.) segmented names. A realm name would be in such structure "data.proactivemkt.taobao".
* It could be concatenated with the object name together to form a full qualified name, e.g. "data.proactivemkt.taobao.customer".
* Please start from <a href="#1b1cc7f086b3f074da452bc3129981eb">query</a> rule.
* </div>
*/
grammar suql;

/**
A SUQL query is constituted for these parts:
<ul>
<li>WITH clause, all the realm name specified here, should be treated as the search space of the object type names,
        aka, if a realm name were specified here, the object name could be directly used in the query; otherwise, an FQN should be used instead.
<li> SELECT clause, it defines the schema of the returning result sets, essentially it is a list of fields separated by commas,
        which you want to retrieve from the specified data type.
        The field name specified here must be valid and have been granted the read permission.
        The difference to SQL lies on the fact it should only specify the fields to the scalar types,
        cause its nested structure and reference relationship.
<li> FROM clause, it is the query target type, it could be Union, Interface and Object types, only one could be specified here. A alias could be specified for further reference.
        A valid type name with read permission should be specified here, or an error will occur.
        If no "WITH clause" were specified or the realm of the datatype lies outside the "WITH clause" specification,
        a full qualified name must be used, e.g. "select ... from data.prctvmkt.taobao.customer as cstr ...".
<li> WHERE clause, it is used to determine which data object in the specified data type to filter against.
        If there's nowhere clause specified, the query will retrieve all data objects which have read permissions in the data type.
<li> group by expression, it is used to group the query results and based on the grouped results and to make a further filter based on the grouped results. it consisted of a GROUP BY clause and an optional HAVING clause.
<li> ORDER BY clause, it specifies a list of fields which used to order the query result.
<li> LIMIT and OFFSET clause, it is used to take a subset of the result set, it defines the first M objects to skip and maximum N objects to take.
</ul>
*/
query
    :   (WITH objectRealmName(',' objectRealmName)*)?
        SELECT selectFieldsExpr
        FROM objectTypeRef ( AS ALIAS )?            //fixME MAY NEED MORE CONSIDERATIONS
        (WHERE logicalExpr)?
        (groupbyExpression)?
        (ORDER_BY fieldExpr (',' fieldExpr)* (ASC | DESC)? (NULLS (FIRST|LAST))?)?
        (LIMIT INTEGER_LITERAL)?
        (OFFSET INTEGER_LITERAL)?
    ;
/**
Selection fields were composed a simple fields list (selectSimpleFieldsExpr) and several polymorphism fields lists (selectPolymorphismFieldsExpr).
If there's a simple fields lists, it must be written first.
<p>
Simple fields list is essentially a list of field names (or equivalent) , separated by commas.
Polimorphism fields list is a field is also a list of fields, but it could return different fields which depend on the type of the object type.
*/
selectFieldsExpr
    :   selectSimpleFieldsExpr (',' selectPolymorphismFieldsExpr)*
    |   selectPolymorphismFieldsExpr (',' selectPolymorphismFieldsExpr)*
    ;

/**
Simple fields list does have only one thing different from SQL, SQL could give alias name to rename the fields of the returning result set.
but in SUQL, a field path were mandatory which were used to give the field name of the returning output result set.
<p>
Since SUQL is dealing with nested data schema and could return nested data schema, field path is instead of field name.
It is also the same with SQL, select fields could be field names, subqueries, func calls, aggregation functions, and literal values.
此处修改
*/
selectSimpleFieldsExpr
    :   selectField (',' selectField)*
    ;
selectField
    :   (valueExpr | funcCall | aggregationExpr | fieldExpr | subquery) ':' fieldPath
    ;

/**
a polymorphism field list is composed of several clauses:
<ul>
<li> type of... clause, it was used to specify the field to determain, if the list is depended on the whole object here, this could be used here.
<li> when... then... clause, if the field/object is an instance of the type specified after "when" keyword,
        the corresponding simple fields list after "then" key word would be used as an part of the returning result set field definition.
        <p>
        Multi when... then... clause could be used for different type determintation.
<li> else... clause, if non of the above type conditions are satisfied, a difault fields list would be supplied.
</ul>
*/
selectPolymorphismFieldsExpr
    :   (TYPE_OF ( THIS | fieldExpr )
            (WHEN type THEN selectSimpleFieldsExpr)+
            (ELSE selectSimpleFieldsExpr)*
         END)
    ;

/**
A subquery could be used in select and where clause. In select clause typically it would form a nested structure of result set schema.
In where clause it mainly used for semi-join and anti-join filter.
<p>
Subqueries don't allow polimophism fields list, group by... having... clause and limit... offset... clause.
A subquery in another subquery is also not supported now ( maybe carried out later)
A subquery could surrounded by a pair of parenthesis.
*/
subquery
    :   '(' subquery ')'
    |   SELECT ( selectFieldsExpr | fieldExpr )
        FROM fieldExpr                                //fixME MAY NEED MORE CONSIDERATIONS, must of type array or a reverse link
        (WHERE logicalExpr)?
    ;

/**
logical expressions could be mainly used in where having clause, it composed of one or multi comparision expressions,
and these compairison expressions are linked together with parenthesis, AND, OR and NOT operaters.
note: if the compairsion expression is a anti-join or a semi-join between data types, it should be in the top level of the logical expression,
aka. if the operation is an AND of multi othere expressions, anti-join / semi-join should be directly one of them, if it has.
If there's a logical expression with AND, OR, NOT in the same time, and didn't have a parenthesis, the composite priority would as : NOT, AND, OR.
*/
logicalExpr
    :   logicalExpr AND logicalExpr
    |   logicalExpr OR logicalExpr
    |   NOT ?  '(' logicalExpr ')'
    |   NOT ? comparisonExpr
    ;

/**
A comparision expression is composed of a operator, one or two operands. The left operands could be a direct field or an aggregation of a field.
If this expression occered in a where.. clause, only direct field could be used, if it occured in a having... cause, an aggregation could also be used.
There're several kinds of operaters:
<ul>
<li> general operators : eg. EQ(equal), NQ(not equal), GT(greater then), LT(less then), GTE(greater then or equal), LTE(less then or equal).
        If these operator were used, a second operation (it should be a literal value) must be specified. These operators could be apply on all the scalar types.
        <br>
        If any type compared with a String type, it would be coerce to string type and a string comparison was taken.
        If a integer was compared to a number, the integer would coerce to a number.
        If it didn't conform the coerce rules above, a error will eccure.
<li> collection operators :
        <ul>
        <li> IN, if the lhv does equal any one of the values specified in the rhv clasuse
        <li> NOT IN, if the lhv does not equal any of the vlues specified in the rhv clause
        <li> INCLUDE ANY, if the lhv does contain any one of the values specified in the rhv clause, in this case, the lhv should be a Array Type of scalars or references.
        <li> INCLUDE ALL, if the lhv does contain all of the values specified in the rhv clause, in this case, the lhv should be a Array Type of scalars or references.
        <li> EXCLUDE ANY, if the lhv does not contain any one or more of the values specified in the rhv clause, in this case, the lhv should be a Array Type of scalars or references.
        <li> EXCLUDE ALL, if the lhv does not contain all of the values specified in the rhv clause, in this case, the lhv should be a Array Type of scalars or references.
        <li> in these cases, rhv clause could be subquery or a value array. please note, subquery is only allowed when lhv is a reference to another object type or an array of reference.
                When rhv clause is a subquery, no explicit join condition could be specified. the join conditions were implicitly specified by the reference it self.
        <li> if the lhv is a scalar or an array of scalar, the rhv clause could only be a value array.`
        <li> if the lhv is a reference or an array of reference, the rhv clause also could be a value array, but a array of IDs, which specified by strings.
        </ul>
<li> INCLUDE / EXCLUDE : means if the lhv does contain / does not contain the rhv. In this case the lhv could only be an array of sclars or references, and the rhv could only be a value expression.
<li> tree operators : tree operator represents the relationship of nodes in a tree. if a tree operator is used here, the left lhv should only be of tree type, or a reference to a tree type,
        and the rhv should only be an ID of a tree node represented by a string.
        <p>
        If an object is of tree type, exactly it represent a node in the tree. and this type could be queried as object type, and also could be queried with the following tree operators.
        <ul>
        <li> CHILD OF, if the lhv is a child of the rhv
        <li> PARENT OF, if the lhv is the parent of the rhv
        <li> DESCENDANT OF, if the lhv is a descendant of the rhv
        <li> ANCESTOR OF, if the lhv is an ancestor of the rhv
        <li> IS LEAF, if the lhv is a leaf node, aka, there no children of it.
        <li> IS ROOT, if the lhv is the root, aka, there no parent to it.
        </ul>
<li> null operators, it was used to judge if the lhv is null or is not null. If the lhv is an array and has no element, the expression should return true.
<li> type operators, if the lhv is of the type specified by the rhv clasue.
        <ul>
            <li> the rhv could be a Scalar type, a Complex type representation, or a type name e.g. is integer, is tree, or is data.prctvmkt.taobao.customer respectively.
                    but do not take interface or union here.
            <li> the lhv could be any thing, if it is a reference to another type, then the referee object was used to evaluate the result.
        </ul>
</ul>
*/
comparisonExpr
    :   filterField ( IN | NOT_IN | INCLUDE_ANY | INCLUDE_ALL | EXCLUDE_ALL | EXCLUDE_ANY ) ( subquery | valueArrayExpr )
    |   filterField ( EQ | NQ | GT | LT | GTE | LTE | LIKE | INCLUDE | EXCLUDE | CHILD_OF | PARENT_OF | ANCESTOR_OF | DESCENDANT_OF ) valueExpr
    |   filterField ( IS_LEAF | IS_ROOT )
    |   filterField ( IS_NULL | IS_NOT_NULL )
    |   filterField ( IS | IS_NOT ) type
    ;

/**
A filter field could be a aggregation expression or just a plain field or THIS.
<br> Aggregation expression could only be used when the filter field is used as part of comparison expression in a having clause or the parameter field is an Array.
<br> THIS denotes the object is which is examined at this point in runtime.
*/
filterField
    :   ( aggregationExpr | fieldExpr | THIS )
    ;

/**
Group by expression consisted of a GROUP BY clause and an optional HAVING clause
<ul>
<li> GROUP BY clause, it is used to group the query results, and specified by a list of fields separated by commas.
        The results could include extra data objects for subtotal of the grouped data, See group by rollup and group by cube grammar.
<li> HAVING clause, it is a optional clause in the group by clause, these condition expressions filters the data objects that group by resturns.
<li> GROUP BY ROLLUP, use the GROUP BY ROLLUP clause to add subtotals to aggregated data in the query result,
        it would enable the query to calculate subtotals so that you you don't have to code it yourself or initiate multi queries.
        The query would return the aggregated data as in the ordinary GROUP BY query, but also return multi levels of subtotal objects (rows).
        the subtotal fields were specified in the comma sparated parameter lists.
        <br>
        The subtotal would be in severval Levels, if there's three were parameter fields, it would be three levels of subtotals:
        <ul>
        <li> 1st level, there would be a result object for every combination of the first and the second field value, the third field would combined.
        <li> 2nd level, there would be a result object for every first field value, the second and third field would be combined.
        <li> 3rd level, one grand total object, in which all three fields would be combined.
        </ul>
        <br>
        eg. the query " select status:status, source:source, count(id):cnt from data.prctvmkt.taobao.order group by rollup(status, source) " would return the following results
        <br>
        <table>
        <tr><th>status</th><th>source</th><th>cnt</th><th>comments</th></tr>
        <tr><td>order_payed</td><td>app</td><td>100</td><td>grouped by status, and source</td><tr>
        <tr><td>order_payed</td><td>desktop</td><td>150</td><td>grouped by status, and source</td><tr>
        <tr><td>order_payed</td><td>null</td><td>250</td><td>grouped by status, first level of subtotal</td><tr>
        <tr><td>order_finished</td><td>app</td><td>200</td><td>grouped by status, and source</td><tr>
        <tr><td>order_finished</td><td>desktop</td><td>250</td><td>grouped by status, and source</td><tr>
        <tr><td>order_finished</td><td>null</td><td>450</td><td>grouped by status, first level of subtotal</td><tr>
        <tr><td>null</td><td>null</td><td>700</td><td>second level of sub total</td><tr>
        </table>

<li> GROUP BY CUBE, use GROUP BY CUBE clause to add subtotals for all combinations of a grouped  field in the query results. It is useful for compiling cross-tabular reports of data.
        It is similar to GROUP BY ROLLUP somewhat, but there are more subtotals.
        <br>
        eg. the query " select status:status, source:source, count(id):cnt from data.prctvmkt.taobao.order group by cube(status, source) " would return the following results
        <br>
        <table>
        <tr><th>status</th><th>source</th><th>cnt</th><th>comments</th></tr>
        <tr><td>order_payed</td><td>app</td><td>100</td><td>grouped by status, and source</td><tr>
        <tr><td>order_payed</td><td>desktop</td><td>150</td><td>grouped by status, and source</td><tr>
        <tr><td>order_finished</td><td>app</td><td>200</td><td>grouped by status, and source</td><tr>
        <tr><td>order_finished</td><td>desktop</td><td>250</td><td>grouped by status, and source</td><tr>
        <tr><td>order_payed</td><td>null</td><td>250</td><td>grouped by status</td><tr>
        <tr><td>order_finished</td><td>null</td><td>450</td><td>grouped by status</td><tr>
        <tr><td>null</td><td>app</td><td>300</td><td>grouped by source</td><tr>
        <tr><td>null</td><td>desktop</td><td>500</td><td>grouped by source</td><tr>
        <tr><td>null</td><td>null</td><td>700</td><td>grand total</td><tr>
        </table>
</ul>
*/
groupbyExpression
    :   GROUP_BY ( fieldExpr(',' fieldExpr)* | ( ROLLUP | CUBE )  '(' fieldExpr (',' fieldExpr)* ')' )
            (HAVING logicalExpr)?
    ;

/**
An aggregation expression is consisted of a aggregation func and an argument.
<br> If the parameter field is an array, and this happens on a query with group by. it would aggregation on the array first, return an array with a value per object in the group.
<br> If the parameter field is just a value, and happens on a query without group by. it would aggregation on the group, and return a value.
<br> If the parameter field is an array, and this happens on a query with group by and you want to an agregation on the array, then on the group, you should have two level of aggreation,
more then two levels of aggregation is not allowed.

*/
aggregationExpr
    :   aggregationFuncName '('( fieldExpr | subquery | aggregationFuncName '(' fieldExpr | subquery  ')' ) ')'
    ;

/**
The aggregation functions include :
<ul>
<li> MAX, to get the max value, if there's null in the target collection, just ommit it.if the target collection is empty or all null, just return null.
    It takes INTEGER, NUMBER, TIME, CURRENCY, STRING.
    If the CURRENCY values were in different currentcy types, it would compare in the company's currency type. the accuracy of the conversion rate were not garenteed.
    If the TIME value will be compared in UTC clock.
<li> MIN, to get the min value, if there's null in the target collection, just ommit it.if the target collection is empty or all null, just return null.
    It takes INTEGER, NUMBER, TIME, CURRENCY, STRING.
    If the CURRENCY values were in different currentcy types, it would compare in the company's currency type. the accuracy of the conversion rate were not garenteed.
    If the TIME value will be compared in UTC clock.
<li> SUM, to get the sum value, if there's null in the target collection, just ommit it.if the target collection is empty or all null, just return null.
    It takes INTEGER, NUMBER, CURRENCY.
    If the CURRENCY values were in different currentcy types, it would calculate in the company's currency type. the accuracy of the conversion rate were not garenteed.
<li> AVERAGE, to get the average of the values, if there's null in the target collection, just ommit it and but it would do take count.if the target collection is empty or all null, just return null.
    It takes INTEGER, NUMBER, CURRENCY.
    If the CURRENCY value were in different currentcy types, it would calculate in the company's currency type. the accuracy of the conversion rate were not garenteed.
<li> COUNT, to get the count of the values, it would count all the values except the null, if all values were null, just return 0.
<li> COUNT_DISTINCT, to get the count of the distinct values, the same with the COUNT, but treat the same values as one. if the target values were reference, multi reference to the same object (identical ID) would be treated as one.
<li> RANGE, the get the Range of the values, aka, the max minus the min. it takes INTEGER, NUMBER, CURRENCY, TIME.
    If the CURRENCY values were in different currentcy types, it would calculate in the company's currency type. the accuracy of the conversion rate were not garenteed.
    if it is the TIME values, the result would be a NUMBER of milliseconds.
<li> ANY, just pick one value, it doesn't garentee the randomness. if there's non null, non null value first.
</ul>
*/
aggregationFuncName
    :   MAX
    |   MIN
    |   SUM
    |   AVERAGE
    |   COUNT
    |   COUNT_DISTINCT
    |   RANGE
    |   ANY
    ;

/**
Func Calls contains a func name a sereral parameters, if there's a field expression, it must take the first parameter.
*/
funcCall
    :   funcName '(' (fieldExpr | valueExpr) (',' valueExpr)* ')'
    ;
/**
Functions include :
<ul>
<li> TO_LABEL, to get the label for current locale setting. It could apply to LABEL type and ENUM type. it there's no localized label found, return the original string instead.
<li> CONVERT_CURRENCY, convert the currency according to the current locale setting.
        <br>it doesn't garentee the accuracy of the conversion rate, and it would always based on the current currency convertion rate which could be maintain by hand or from a external interface.
<li> CURRENCY_TYPE, get the currency type of this currency. the result would be a CURRENCY TYPE.
<li> CURRENCY_VALUE, get the currency value of this currency, the result would be a NNUMBER value.
<li> CONVERT_TZ, get the time value correcponding to the locale setting of the current user.
<li> CALENDRA_MONTH, CALENDRA_QUATER, CALENDRA_YEAR, FISCAL_MONTH, FISCAL_QUATER, FISCAL_YEAR, get the calendra month, quater, year and fiscal month, quater, year of the time value specified in parameter, respectively.
        <br> the result would be a INTEGER value. note, the year would be in four digitals. since no bce time supported, no neganive result year value there would be.
        <br> month values and quater values are all started from 1, aka, January and spring season would be designated to 1,
<li> DAY_IN_MONTH, DAY_IN_WEEK, WEEK_IN_MONTH, WEEK_IN_YEAR, get the day in month, day in week, week in month and week in year of the time value specified in parameter, respectively.
        <br> the result would be a INTEGER value. day in week, day in month, week in month and week in year are all started from 1.
<li> TS, get the timstamp of the time value specified in the parameter. the result would be a INTEGER value which represents milliseconds from 1970-01-01T00:00:00.000Z to the time specified.
<li> TZ, get the timezone of the time value specified in the parameter. the result would be a TZ value. Since the time type conforms to ISO8601, the return value would represont a Time offset rather then a tz name.
<li> GROUPING, the parameter for GROUPING could be only a plain field, the func would return if this field the query is grouped by. This func is only allowed to use with GROUP BY ROLLUP or GROUP BY CUBIC clause.
</ul>
*/
funcName
    :   TO_LABEL
    |   CONVERT_CURRENCY
    |   CURRENCY_TYPE
    |   CURRENCY_VALUE
    |   CONVERT_TZ
    |   CALENDRA_MONTH
    |   CALENDRA_QUARTER
    |   CALENDRA_YEAR
    |   FISCAL_MONTH
    |   FISCAL_QUARTER
    |   FISCAL_YEAR
    |   DAY_IN_MONTH
    |   DAY_IN_WEEK
    |   WEEK_IN_MONTH
    |   WEEK_IN_YEAR
    |   TS
    |   TZ
    |   GROUPING
    ;


/**
A field expression typically specify a field. Without ambiguity, it could be just a field path.
If there's an ambibuity or for clearness, object type prefix should be given. The object type prefix could be
<ul>
<li> THIS, the object which is examined in the current query context.
<li> OBJECT_TYPE_NAME, it could only be
        <ul>
        <li> the type name of the current query, in this case it has the same meaning with THIS.
        <li> the type name of the upper level queries, if this is occured in a subquery.
        </ul>
<li> ALIAS, which was defined in the FROM clause.
</ul>
Since the objec type in data service is in a nested structure,
the field path could be a multi segment path, separated by slash ('/').if there's a array embeded in, array elements could not be specified by this path, use subquery instead if needed.
<p>
If the field expression was used as a direct selection field, group by field and order by field, it could only be of scalar types.
If the field expression was used as the aggregation function parameter, it could be of array of scalars, or just a scalar value(if with a GROUP BY clause).
If the field expression was used as a function parameter, the type allowd was defined by the the func name.
*/
fieldExpr
    :   ((THIS | objectTypeRef)'.')? fieldPath ; // JUst  mean how to reference to a field

fieldPath
    :   fieldName('/'fieldName)*
    ;
fieldName
    : NAME
    ;   // the field name only

objectTypeRef
    :   objectTypeFqn
    |   objectTypeName
    |   ALIAS
    ;

/**
A type name could be just be a simple type name or a full qualified name (FQN) which constists a realm name and a simple type name.
if the realm was declared in the WITH clause, simple type name could be used, other wise FQN should be used.
note, the declaration should be the full realm name, part of the realm name is meaningless.
the realm name here should conform this rule:
<ul>
<li> it has multi segment, separated with dot('.')
<li> it must start with 'data'.
<li> the segment after the 'data' should be the the name / code of the application / service who owned the data type.
<li> other segments should be as namespace defined by the application corresponding to business requirments.
</ul>
*/
objectTypeFqn
    :   objectRealmName + objectTypeName
    ;
objectRealmName
    :   DATA ('.'realm)+
    ;
realm
    : NAME
    ;
objectTypeName
    : NAME
    ;
valueArrayExpr
    :   '(' (valueExpr (','valueExpr)* )? ')'
    ;

valueExpr
    :   INTEGER_LITERAL
    |   NUMBER_LITERAL
    |   STRING_LITERAL
    |   TREE
    |   FALSE
    |   timeRangeLiteral //todo should add value expressions on how to express a time point / date point, fiscalyear, etc
    |   TIME_LITERAL
    |   NULL
    ;

type            : scaleType
                | complexType
                | objectTypeRef
                ;

scaleType       : ID
                | INTEGER
                | NUMBER
                | STRING
                | BOOLEAN
                | LABEL
                | PHONE_NO
                | EMAIL
                | CURRENCY
                | CURRENCY_TYPE
                | TIME
                | TIME_RANGE
                | TZ
                | URL
                | ENUM
                ;

complexType     : INTERFACE
                | OBJECT
                | ARRAY
                | UNION
                | TREE
                ;

timeRangeLiteral
    :   YESTERDAY
    |   TODAY
    |   TOMORROW
    |   LAST_WEEK
    |   NEXT_WEEK
    |   THIS_WEEK
    |   THIS_MONTH
    |   LAST_MONTH
    |   NEXT_MONTH
    |   LAST_90_DAYS
    |   NEXT_90_DAYS
    |   LAST_N_DAYS ':' INTEGER_LITERAL
    |   NEXT_N_DAYS ':' INTEGER_LITERAL
    |   LAST_N_WEEKS ':' INTEGER_LITERAL
    |   NEXT_N_WEEKS ':' INTEGER_LITERAL
    |   LAST_N_MONTHS ':' INTEGER_LITERAL
    |   NEXT_N_MONTHS ':' INTEGER_LITERAL
    |   NEXT_QUARTER
    |   THIS_QUARTER
    |   LAST_QUARTER
    |   NEXT_YEAR
    |   THIS_YEAR
    |   LAST_YEAR
    |   NEXT_N_QUARTERS ':' INTEGER_LITERAL
    |   LAST_N_QUARTERS ':' INTEGER_LITERAL
    |   NEXT_N_YEARS ':' INTEGER_LITERAL
    |   LAST_N_YEARS ':' INTEGER_LITERAL
    |   THIS_FISCAL_QUARTER
    |   LAST_FISCAL_QUARTER
    |   NEXT_FISCAL_QUARTER
    |   THIS_FISCAL_YEAR
    |   LAST_FISCAL_YEAR
    |   NEXT_FISCAL_YEAR
    |   LAST_N_FISCAL_QUARTERS ':' INTEGER_LITERAL
    |   NEXT_N_FISCAL_QUARTERS ':' INTEGER_LITERAL
    |   LAST_N_FISCAL_YEARS ':' INTEGER_LITERAL
    |   NEXT_N_FISCAL_YEARS ':' INTEGER_LITERAL
    ;
/* LEXER RULES */

/* KEY WORDS */
WITH            : W I T H;
SELECT          : S E L E C T;
FROM            : F R O M;
WHERE           : W H E R E;
GROUP_BY        : G R O U P WS B Y;
HAVING          : H A V I N G;
ORDER_BY        : O R D E R WS B Y;
ASC             : A S C;
DESC            : D E S C;
FIRST           : F I R S T;
LAST            : L A S T;
LIMIT           : L I M I T;
OFFSET          : O F F S E T;

ROLLUP          : R O L L U P;
CUBE            : C U B E;

NULL            : N U L L;
NULLS           : N U L L S;

NOT             : N O T;
IS_NULL         : I S WS N U L L;
IS_NOT_NULL     : I S WS N O T WS N U L L;

ID              : I D;
INTEGER         : I N T E G E R;
NUMBER          : N U M B E R;
STRING          : S T R I N G;
BOOLEAN         : B O O L E A N;
LABEL           : L A B E L;
PHONE_NO        : P H O N E '_' N O;
EMAIL           : E M A I L;
CURRENCY        : C U R R E N C Y;
CURRENCY_TYPE   : C U R R E N C Y '_' T Y P E;
CURRENCY_VALUE  : C U R R E N C Y '_' V A L U E;
TIME            : T I M E;
TIME_RANGE      : T I M E '_' R A N G E;
TZ              : T Z;
URL             : U R L;

OBJECT          : O B J E C T;
TREE            : T R E E;
ENUM            : E N U M;
INTERFACE       : I N T E R F A C E;
ARRAY           : A R R A Y;
UNION           : U N I O N;

GROUPING        : G R O U P I N G;

AS              : A S;

DATA            : D A T A;


NAME            : [a-zA-Z][a-zA-Z_]+ ;
ALIAS           : '$'[a-zA-Z][a-zA-Z_]+ ;

AND             : A N D | '&&';
OR              : O R | '||';

EQ              : E Q | '=';
NQ              : N Q | '!=';
GT              : G T | '>';
LT              : L T | '<';
GTE             : G T E | '>=';
LTE             : L T E | '<=';
LIKE            : L I K E ;
IN              : I N;
NOT_IN          : N O T WS I N;
INCLUDE         : I N C L U D E;
EXCLUDE         : E X C L U D E;
INCLUDE_ANY      : I N C L U D E WS A N Y;
INCLUDE_ALL      : I N C L U D E WS A L L;
EXCLUDE_ANY      : E X C L U D E WS A N Y;
EXCLUDE_ALL      : E X C L U D E WS A L L;
CHILD_OF        : C H I L D WS O F;
PARENT_OF       : P A R E N T WS O F;
ANCESTOR_OF     : A N C E S T O R WS O F;
DESCENDANT_OF   : D E S C E N D A N T WS O F;
IS_LEAF         : I S WS L E A F;
IS_ROOT         : I S WS R O O T;

IS              : I S;
IS_NOT          : I S WS N O T;

TYPE_OF         : T Y P E WS O F;
WHEN            : W H E N;
THEN            : T H E N;
ELSE            : E L S E;
END             : E N D;

THIS            : T H I S;

MAX             : M A X;
MIN             : M I N;
SUM             : S U M;
AVERAGE         : A V E R A G E;
COUNT           : C O U N T;
COUNT_DISTINCT  : C O U N T '_' D I S T I N C T;
RANGE           : R A N G E;
ANY             : A N Y;

TO_LABEL        : T O '_' L A B E L;
CONVERT_CURRENCY : C O N V E R T '_' C U R R E N C Y;
CONVERT_TZ      : C O N V E R T '_' T Z;

CALENDRA_MONTH  : C A L E N D R A '_' M O N T H;
CALENDRA_QUARTER: C A L E N D R A '_' Q U A R T E R;
CALENDRA_YEAR   : C A L E N D R A '_' Y E A R;
FISCAL_MONTH    : F I S C A L '_' M O N T H;
FISCAL_QUARTER  : F I S C A L '_' Q U A R T E R;
FISCAL_YEAR     : F I S C A L '_' Y E A R;
DAY_IN_MONTH    : D A Y '_' I N '_' M O N T H;
DAY_IN_WEEK     : D A Y '_' I N '_' W E E K;
WEEK_IN_MONTH   : W E E K '_' I N '_' M O N T H;
WEEK_IN_YEAR    : W E E K '_' I N '_' Y E A R;

TS              : T S;

TRUE            : T R U E;
FALSE           : F A L S E;

YESTERDAY       : Y E S T E R D A Y;
TODAY           : T O D A Y;
TOMORROW        : T O M O R R O W;
LAST_WEEK       : L A S T '_' W E E K;
THIS_WEEK       : T H I S '_' W E E K;
NEXT_WEEK       : N E X T '_' W E E K;
LAST_MONTH      : L A S T '_' M O N T H;
THIS_MONTH      : T H I S '_' M O N T H;
NEXT_MONTH      : N E X T '_' M O N T H;
LAST_90_DAYS    : L A S T '_' '90' '_' D A Y S;
NEXT_90_DAYS    : N E X T '_' '90' '_' D A Y S;
LAST_N_DAYS     : L A S T '_' N '_' D A Y S;
NEXT_N_DAYS     : N E X T '_' N '_' D A Y S;
LAST_N_WEEKS    : L A S T '_' N '_' W E E K S;
NEXT_N_WEEKS    : N E X T '_' N '_' W E E K S;
LAST_N_MONTHS   : L A S T '_' N '_' M O N T H S;
NEXT_N_MONTHS   : N E X T '_' N '_' M O N T H S;
NEXT_QUARTER    : N E X T '_' Q U A R T E R;
THIS_QUARTER    : T H I S '_' Q U A R T E R;
LAST_QUARTER    : L A S T '_' Q U A R T E R;
THIS_YEAR       : T H I S '_' Y E A R;
LAST_YEAR       : L A S T '_' Y E A R;
NEXT_YEAR       : N E X T '_' Y E A R;
LAST_N_QUARTERS : L A S T '_' N '_' Q U A R T E R S;
NEXT_N_QUARTERS : N E X T '_' N '_' Q U A R T E R S;
LAST_N_YEARS    : L A S T '_' N '_' Y E A R S;
NEXT_N_YEARS    : N E X T '_' N '_' Y E A R S;
THIS_FISCAL_QUARTER     : T H I S '_' F I S C A L '_' Q U A R T E R;
LAST_FISCAL_QUARTER     : L A S T '_' F I S C A L '_' Q U A R T E R;
NEXT_FISCAL_QUARTER     : N E X T '_' F I S C A L '_' Q U A R T E R;
THIS_FISCAL_YEAR        : T H I S '_' F I S C A L '_' Y E A R;
LAST_FISCAL_YEAR        : L A S T '_' F I S C A L '_' Y E A R;
NEXT_FISCAL_YEAR        : N E X T '_' F I S C A L '_' Y E A R;
LAST_N_FISCAL_QUARTERS  : L A S T '_' N '_' F I S C A L '_' Q U A R T E R S;
NEXT_N_FISCAL_QUARTERS  : N E X T '_' N '_' F I S C A L '_' Q U A R T E R S;
LAST_N_FISCAL_YEARS     : L A S T '_' N '_' F I S C A L '_' Y E A R S;
NEXT_N_FISCAL_YEARS     : N E X T '_' N '_' F I S C A L '_' Y E A R S;



INTEGER_LITERAL     : '-'? INT;
NUMBER_LITERAL      : '-'? INT '.' [0-9]+ EXP? | '-'? INT EXP;
STRING_LITERAL      : ('"'(ESC | ~ ["\\])* '"') | ('\''(ESC | ~ ['\\])* '\'');

/**
Time literals should be in ISO8601 format, eg. 1970-02-21T12:12:32.000Z . please refer to ISO 8601 standard.
*/
TIME_LITERAL        : [0-9][0-9][0-9][0-9]'-'[01][0-9]'-'[0-3][0-9]
                        ('T'[0-2][0-9]':'[0-5][0-9]('.'[0-9][0-9][0-9])?)?
                        ('Z' | ('+'|'-')[0-1][0-9]':'[0-5][0-9])?                   // if the tz data is absent, use the default tz data.
                    ;

/**
Comments could be write after '//' to the end of the line as line comment,
or between '/*' and '\htmlonly&#42;/\endhtmlonly\latexonly$\ast$/\endlatexonly' as block comment.
*/
COMMENT     : '/*' .*? '*/' -> skip;
LINE_COMMENT: '//' ~[\r\n]* -> skip;

/**
White spaces could be ommitted in the SUQL script. any syntax elements could be write in the any place of the script text.
*/
WHITESPACES          : WS -> skip;

fragment WS         : [ \t\n\r] + ;


fragment INT        : '0' | [1-9] [0-9]*;
fragment EXP        : [Ee] [+/-]? INT;
fragment ESC        : '\\' (["'\\/bfnrt] | UNICODE);
fragment UNICODE    : 'u' HEX HEX HEX HEX;
fragment HEX        : [0-9a-fA-F];

/**
For keywords in the SUQL, uppercases and lowercases are irrelivant. eg. 'SELECT' also could be 'select' or 'SeLeCt'.
*/
fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
