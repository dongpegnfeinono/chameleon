// Generated from suql.g4 by ANTLR 4.5.3
package com.shuyun.chameleon.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class suqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, WITH=7, SELECT=8, FROM=9, 
		WHERE=10, GROUP_BY=11, HAVING=12, ORDER_BY=13, ASC=14, DESC=15, FIRST=16, 
		LAST=17, LIMIT=18, OFFSET=19, ROLLUP=20, CUBE=21, NULL=22, NULLS=23, NOT=24, 
		IS_NULL=25, IS_NOT_NULL=26, ID=27, INTEGER=28, NUMBER=29, STRING=30, BOOLEAN=31, 
		LABEL=32, PHONE_NO=33, EMAIL=34, CURRENCY=35, CURRENCY_TYPE=36, CURRENCY_VALUE=37, 
		TIME=38, TIME_RANGE=39, TZ=40, URL=41, OBJECT=42, TREE=43, ENUM=44, INTERFACE=45, 
		ARRAY=46, UNION=47, GROUPING=48, AS=49, DATA=50, NAME=51, ALIAS=52, AND=53, 
		OR=54, EQ=55, NQ=56, GT=57, LT=58, GTE=59, LTE=60, LIKE=61, IN=62, NOT_IN=63, 
		INCLUDE=64, EXCLUDE=65, INCLUDE_ANY=66, INCLUDE_ALL=67, EXCLUDE_ANY=68, 
		EXCLUDE_ALL=69, CHILD_OF=70, PARENT_OF=71, ANCESTOR_OF=72, DESCENDANT_OF=73, 
		IS_LEAF=74, IS_ROOT=75, IS=76, IS_NOT=77, TYPE_OF=78, WHEN=79, THEN=80, 
		ELSE=81, END=82, THIS=83, MAX=84, MIN=85, SUM=86, AVERAGE=87, COUNT=88, 
		COUNT_DISTINCT=89, RANGE=90, ANY=91, TO_LABEL=92, CONVERT_CURRENCY=93, 
		CONVERT_TZ=94, CALENDRA_MONTH=95, CALENDRA_QUARTER=96, CALENDRA_YEAR=97, 
		FISCAL_MONTH=98, FISCAL_QUARTER=99, FISCAL_YEAR=100, DAY_IN_MONTH=101, 
		DAY_IN_WEEK=102, WEEK_IN_MONTH=103, WEEK_IN_YEAR=104, TS=105, TRUE=106, 
		FALSE=107, YESTERDAY=108, TODAY=109, TOMORROW=110, LAST_WEEK=111, THIS_WEEK=112, 
		NEXT_WEEK=113, LAST_MONTH=114, THIS_MONTH=115, NEXT_MONTH=116, LAST_90_DAYS=117, 
		NEXT_90_DAYS=118, LAST_N_DAYS=119, NEXT_N_DAYS=120, LAST_N_WEEKS=121, 
		NEXT_N_WEEKS=122, LAST_N_MONTHS=123, NEXT_N_MONTHS=124, NEXT_QUARTER=125, 
		THIS_QUARTER=126, LAST_QUARTER=127, THIS_YEAR=128, LAST_YEAR=129, NEXT_YEAR=130, 
		LAST_N_QUARTERS=131, NEXT_N_QUARTERS=132, LAST_N_YEARS=133, NEXT_N_YEARS=134, 
		THIS_FISCAL_QUARTER=135, LAST_FISCAL_QUARTER=136, NEXT_FISCAL_QUARTER=137, 
		THIS_FISCAL_YEAR=138, LAST_FISCAL_YEAR=139, NEXT_FISCAL_YEAR=140, LAST_N_FISCAL_QUARTERS=141, 
		NEXT_N_FISCAL_QUARTERS=142, LAST_N_FISCAL_YEARS=143, NEXT_N_FISCAL_YEARS=144, 
		INTEGER_LITERAL=145, NUMBER_LITERAL=146, STRING_LITERAL=147, TIME_LITERAL=148, 
		COMMENT=149, LINE_COMMENT=150, WHITESPACES=151;
	public static final int
		RULE_query = 0, RULE_selectFieldsExpr = 1, RULE_selectSimpleFieldsExpr = 2, 
		RULE_selectField = 3, RULE_selectPolymorphismFieldsExpr = 4, RULE_subquery = 5, 
		RULE_logicalExpr = 6, RULE_comparisonExpr = 7, RULE_filterField = 8, RULE_groupbyExpression = 9, 
		RULE_aggregationExpr = 10, RULE_aggregationFuncName = 11, RULE_funcCall = 12, 
		RULE_funcName = 13, RULE_fieldExpr = 14, RULE_fieldPath = 15, RULE_fieldName = 16, 
		RULE_objectTypeRef = 17, RULE_objectTypeFqn = 18, RULE_objectRealmName = 19, 
		RULE_realm = 20, RULE_objectTypeName = 21, RULE_valueArrayExpr = 22, RULE_valueExpr = 23, 
		RULE_type = 24, RULE_scaleType = 25, RULE_complexType = 26, RULE_timeRangeLiteral = 27;
	public static final String[] ruleNames = {
		"query", "selectFieldsExpr", "selectSimpleFieldsExpr", "selectField", 
		"selectPolymorphismFieldsExpr", "subquery", "logicalExpr", "comparisonExpr", 
		"filterField", "groupbyExpression", "aggregationExpr", "aggregationFuncName", 
		"funcCall", "funcName", "fieldExpr", "fieldPath", "fieldName", "objectTypeRef", 
		"objectTypeFqn", "objectRealmName", "realm", "objectTypeName", "valueArrayExpr", 
		"valueExpr", "type", "scaleType", "complexType", "timeRangeLiteral"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "','", "':'", "'('", "')'", "'.'", "'/'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, "WITH", "SELECT", "FROM", "WHERE", 
		"GROUP_BY", "HAVING", "ORDER_BY", "ASC", "DESC", "FIRST", "LAST", "LIMIT", 
		"OFFSET", "ROLLUP", "CUBE", "NULL", "NULLS", "NOT", "IS_NULL", "IS_NOT_NULL", 
		"ID", "INTEGER", "NUMBER", "STRING", "BOOLEAN", "LABEL", "PHONE_NO", "EMAIL", 
		"CURRENCY", "CURRENCY_TYPE", "CURRENCY_VALUE", "TIME", "TIME_RANGE", "TZ", 
		"URL", "OBJECT", "TREE", "ENUM", "INTERFACE", "ARRAY", "UNION", "GROUPING", 
		"AS", "DATA", "NAME", "ALIAS", "AND", "OR", "EQ", "NQ", "GT", "LT", "GTE", 
		"LTE", "LIKE", "IN", "NOT_IN", "INCLUDE", "EXCLUDE", "INCLUDE_ANY", "INCLUDE_ALL", 
		"EXCLUDE_ANY", "EXCLUDE_ALL", "CHILD_OF", "PARENT_OF", "ANCESTOR_OF", 
		"DESCENDANT_OF", "IS_LEAF", "IS_ROOT", "IS", "IS_NOT", "TYPE_OF", "WHEN", 
		"THEN", "ELSE", "END", "THIS", "MAX", "MIN", "SUM", "AVERAGE", "COUNT", 
		"COUNT_DISTINCT", "RANGE", "ANY", "TO_LABEL", "CONVERT_CURRENCY", "CONVERT_TZ", 
		"CALENDRA_MONTH", "CALENDRA_QUARTER", "CALENDRA_YEAR", "FISCAL_MONTH", 
		"FISCAL_QUARTER", "FISCAL_YEAR", "DAY_IN_MONTH", "DAY_IN_WEEK", "WEEK_IN_MONTH", 
		"WEEK_IN_YEAR", "TS", "TRUE", "FALSE", "YESTERDAY", "TODAY", "TOMORROW", 
		"LAST_WEEK", "THIS_WEEK", "NEXT_WEEK", "LAST_MONTH", "THIS_MONTH", "NEXT_MONTH", 
		"LAST_90_DAYS", "NEXT_90_DAYS", "LAST_N_DAYS", "NEXT_N_DAYS", "LAST_N_WEEKS", 
		"NEXT_N_WEEKS", "LAST_N_MONTHS", "NEXT_N_MONTHS", "NEXT_QUARTER", "THIS_QUARTER", 
		"LAST_QUARTER", "THIS_YEAR", "LAST_YEAR", "NEXT_YEAR", "LAST_N_QUARTERS", 
		"NEXT_N_QUARTERS", "LAST_N_YEARS", "NEXT_N_YEARS", "THIS_FISCAL_QUARTER", 
		"LAST_FISCAL_QUARTER", "NEXT_FISCAL_QUARTER", "THIS_FISCAL_YEAR", "LAST_FISCAL_YEAR", 
		"NEXT_FISCAL_YEAR", "LAST_N_FISCAL_QUARTERS", "NEXT_N_FISCAL_QUARTERS", 
		"LAST_N_FISCAL_YEARS", "NEXT_N_FISCAL_YEARS", "INTEGER_LITERAL", "NUMBER_LITERAL", 
		"STRING_LITERAL", "TIME_LITERAL", "COMMENT", "LINE_COMMENT", "WHITESPACES"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "suql.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public suqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class QueryContext extends ParserRuleContext {
		public TerminalNode SELECT() { return getToken(suqlParser.SELECT, 0); }
		public SelectFieldsExprContext selectFieldsExpr() {
			return getRuleContext(SelectFieldsExprContext.class,0);
		}
		public TerminalNode FROM() { return getToken(suqlParser.FROM, 0); }
		public ObjectTypeRefContext objectTypeRef() {
			return getRuleContext(ObjectTypeRefContext.class,0);
		}
		public TerminalNode WITH() { return getToken(suqlParser.WITH, 0); }
		public List<ObjectRealmNameContext> objectRealmName() {
			return getRuleContexts(ObjectRealmNameContext.class);
		}
		public ObjectRealmNameContext objectRealmName(int i) {
			return getRuleContext(ObjectRealmNameContext.class,i);
		}
		public TerminalNode AS() { return getToken(suqlParser.AS, 0); }
		public TerminalNode ALIAS() { return getToken(suqlParser.ALIAS, 0); }
		public TerminalNode WHERE() { return getToken(suqlParser.WHERE, 0); }
		public LogicalExprContext logicalExpr() {
			return getRuleContext(LogicalExprContext.class,0);
		}
		public GroupbyExpressionContext groupbyExpression() {
			return getRuleContext(GroupbyExpressionContext.class,0);
		}
		public TerminalNode ORDER_BY() { return getToken(suqlParser.ORDER_BY, 0); }
		public List<FieldExprContext> fieldExpr() {
			return getRuleContexts(FieldExprContext.class);
		}
		public FieldExprContext fieldExpr(int i) {
			return getRuleContext(FieldExprContext.class,i);
		}
		public TerminalNode LIMIT() { return getToken(suqlParser.LIMIT, 0); }
		public List<TerminalNode> INTEGER_LITERAL() { return getTokens(suqlParser.INTEGER_LITERAL); }
		public TerminalNode INTEGER_LITERAL(int i) {
			return getToken(suqlParser.INTEGER_LITERAL, i);
		}
		public TerminalNode OFFSET() { return getToken(suqlParser.OFFSET, 0); }
		public TerminalNode NULLS() { return getToken(suqlParser.NULLS, 0); }
		public TerminalNode ASC() { return getToken(suqlParser.ASC, 0); }
		public TerminalNode DESC() { return getToken(suqlParser.DESC, 0); }
		public TerminalNode FIRST() { return getToken(suqlParser.FIRST, 0); }
		public TerminalNode LAST() { return getToken(suqlParser.LAST, 0); }
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitQuery(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_query);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(56);
				match(WITH);
				setState(57);
				objectRealmName();
				setState(62);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(58);
					match(T__0);
					setState(59);
					objectRealmName();
					}
					}
					setState(64);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(67);
			match(SELECT);
			setState(68);
			selectFieldsExpr();
			setState(69);
			match(FROM);
			setState(70);
			objectTypeRef();
			setState(73);
			_la = _input.LA(1);
			if (_la==AS) {
				{
				setState(71);
				match(AS);
				setState(72);
				match(ALIAS);
				}
			}

			setState(77);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(75);
				match(WHERE);
				setState(76);
				logicalExpr(0);
				}
			}

			setState(80);
			_la = _input.LA(1);
			if (_la==GROUP_BY) {
				{
				setState(79);
				groupbyExpression();
				}
			}

			setState(98);
			_la = _input.LA(1);
			if (_la==ORDER_BY) {
				{
				setState(82);
				match(ORDER_BY);
				setState(83);
				fieldExpr();
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(84);
					match(T__0);
					setState(85);
					fieldExpr();
					}
					}
					setState(90);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(92);
				_la = _input.LA(1);
				if (_la==ASC || _la==DESC) {
					{
					setState(91);
					_la = _input.LA(1);
					if ( !(_la==ASC || _la==DESC) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					}
				}

				setState(96);
				_la = _input.LA(1);
				if (_la==NULLS) {
					{
					setState(94);
					match(NULLS);
					setState(95);
					_la = _input.LA(1);
					if ( !(_la==FIRST || _la==LAST) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					}
				}

				}
			}

			setState(102);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(100);
				match(LIMIT);
				setState(101);
				match(INTEGER_LITERAL);
				}
			}

			setState(106);
			_la = _input.LA(1);
			if (_la==OFFSET) {
				{
				setState(104);
				match(OFFSET);
				setState(105);
				match(INTEGER_LITERAL);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectFieldsExprContext extends ParserRuleContext {
		public SelectSimpleFieldsExprContext selectSimpleFieldsExpr() {
			return getRuleContext(SelectSimpleFieldsExprContext.class,0);
		}
		public List<SelectPolymorphismFieldsExprContext> selectPolymorphismFieldsExpr() {
			return getRuleContexts(SelectPolymorphismFieldsExprContext.class);
		}
		public SelectPolymorphismFieldsExprContext selectPolymorphismFieldsExpr(int i) {
			return getRuleContext(SelectPolymorphismFieldsExprContext.class,i);
		}
		public SelectFieldsExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectFieldsExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterSelectFieldsExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitSelectFieldsExpr(this);
		}
	}

	public final SelectFieldsExprContext selectFieldsExpr() throws RecognitionException {
		SelectFieldsExprContext _localctx = new SelectFieldsExprContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_selectFieldsExpr);
		int _la;
		try {
			setState(124);
			switch (_input.LA(1)) {
			case T__2:
			case SELECT:
			case NULL:
			case CURRENCY_TYPE:
			case CURRENCY_VALUE:
			case TZ:
			case TREE:
			case GROUPING:
			case DATA:
			case NAME:
			case ALIAS:
			case THIS:
			case MAX:
			case MIN:
			case SUM:
			case AVERAGE:
			case COUNT:
			case COUNT_DISTINCT:
			case RANGE:
			case ANY:
			case TO_LABEL:
			case CONVERT_CURRENCY:
			case CONVERT_TZ:
			case CALENDRA_MONTH:
			case CALENDRA_QUARTER:
			case CALENDRA_YEAR:
			case FISCAL_MONTH:
			case FISCAL_QUARTER:
			case FISCAL_YEAR:
			case DAY_IN_MONTH:
			case DAY_IN_WEEK:
			case WEEK_IN_MONTH:
			case WEEK_IN_YEAR:
			case TS:
			case FALSE:
			case YESTERDAY:
			case TODAY:
			case TOMORROW:
			case LAST_WEEK:
			case THIS_WEEK:
			case NEXT_WEEK:
			case LAST_MONTH:
			case THIS_MONTH:
			case NEXT_MONTH:
			case LAST_90_DAYS:
			case NEXT_90_DAYS:
			case LAST_N_DAYS:
			case NEXT_N_DAYS:
			case LAST_N_WEEKS:
			case NEXT_N_WEEKS:
			case LAST_N_MONTHS:
			case NEXT_N_MONTHS:
			case NEXT_QUARTER:
			case THIS_QUARTER:
			case LAST_QUARTER:
			case THIS_YEAR:
			case LAST_YEAR:
			case NEXT_YEAR:
			case LAST_N_QUARTERS:
			case NEXT_N_QUARTERS:
			case LAST_N_YEARS:
			case NEXT_N_YEARS:
			case THIS_FISCAL_QUARTER:
			case LAST_FISCAL_QUARTER:
			case NEXT_FISCAL_QUARTER:
			case THIS_FISCAL_YEAR:
			case LAST_FISCAL_YEAR:
			case NEXT_FISCAL_YEAR:
			case LAST_N_FISCAL_QUARTERS:
			case NEXT_N_FISCAL_QUARTERS:
			case LAST_N_FISCAL_YEARS:
			case NEXT_N_FISCAL_YEARS:
			case INTEGER_LITERAL:
			case NUMBER_LITERAL:
			case STRING_LITERAL:
			case TIME_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(108);
				selectSimpleFieldsExpr();
				setState(113);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(109);
					match(T__0);
					setState(110);
					selectPolymorphismFieldsExpr();
					}
					}
					setState(115);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case TYPE_OF:
				enterOuterAlt(_localctx, 2);
				{
				setState(116);
				selectPolymorphismFieldsExpr();
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(117);
					match(T__0);
					setState(118);
					selectPolymorphismFieldsExpr();
					}
					}
					setState(123);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectSimpleFieldsExprContext extends ParserRuleContext {
		public List<SelectFieldContext> selectField() {
			return getRuleContexts(SelectFieldContext.class);
		}
		public SelectFieldContext selectField(int i) {
			return getRuleContext(SelectFieldContext.class,i);
		}
		public SelectSimpleFieldsExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectSimpleFieldsExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterSelectSimpleFieldsExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitSelectSimpleFieldsExpr(this);
		}
	}

	public final SelectSimpleFieldsExprContext selectSimpleFieldsExpr() throws RecognitionException {
		SelectSimpleFieldsExprContext _localctx = new SelectSimpleFieldsExprContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_selectSimpleFieldsExpr);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			selectField();
			setState(131);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(127);
					match(T__0);
					setState(128);
					selectField();
					}
					}
				}
				setState(133);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectFieldContext extends ParserRuleContext {
		public FieldPathContext fieldPath() {
			return getRuleContext(FieldPathContext.class,0);
		}
		public ValueExprContext valueExpr() {
			return getRuleContext(ValueExprContext.class,0);
		}
		public FuncCallContext funcCall() {
			return getRuleContext(FuncCallContext.class,0);
		}
		public AggregationExprContext aggregationExpr() {
			return getRuleContext(AggregationExprContext.class,0);
		}
		public FieldExprContext fieldExpr() {
			return getRuleContext(FieldExprContext.class,0);
		}
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public SelectFieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectField; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterSelectField(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitSelectField(this);
		}
	}

	public final SelectFieldContext selectField() throws RecognitionException {
		SelectFieldContext _localctx = new SelectFieldContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_selectField);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(139);
			switch (_input.LA(1)) {
			case NULL:
			case TREE:
			case FALSE:
			case YESTERDAY:
			case TODAY:
			case TOMORROW:
			case LAST_WEEK:
			case THIS_WEEK:
			case NEXT_WEEK:
			case LAST_MONTH:
			case THIS_MONTH:
			case NEXT_MONTH:
			case LAST_90_DAYS:
			case NEXT_90_DAYS:
			case LAST_N_DAYS:
			case NEXT_N_DAYS:
			case LAST_N_WEEKS:
			case NEXT_N_WEEKS:
			case LAST_N_MONTHS:
			case NEXT_N_MONTHS:
			case NEXT_QUARTER:
			case THIS_QUARTER:
			case LAST_QUARTER:
			case THIS_YEAR:
			case LAST_YEAR:
			case NEXT_YEAR:
			case LAST_N_QUARTERS:
			case NEXT_N_QUARTERS:
			case LAST_N_YEARS:
			case NEXT_N_YEARS:
			case THIS_FISCAL_QUARTER:
			case LAST_FISCAL_QUARTER:
			case NEXT_FISCAL_QUARTER:
			case THIS_FISCAL_YEAR:
			case LAST_FISCAL_YEAR:
			case NEXT_FISCAL_YEAR:
			case LAST_N_FISCAL_QUARTERS:
			case NEXT_N_FISCAL_QUARTERS:
			case LAST_N_FISCAL_YEARS:
			case NEXT_N_FISCAL_YEARS:
			case INTEGER_LITERAL:
			case NUMBER_LITERAL:
			case STRING_LITERAL:
			case TIME_LITERAL:
				{
				setState(134);
				valueExpr();
				}
				break;
			case CURRENCY_TYPE:
			case CURRENCY_VALUE:
			case TZ:
			case GROUPING:
			case TO_LABEL:
			case CONVERT_CURRENCY:
			case CONVERT_TZ:
			case CALENDRA_MONTH:
			case CALENDRA_QUARTER:
			case CALENDRA_YEAR:
			case FISCAL_MONTH:
			case FISCAL_QUARTER:
			case FISCAL_YEAR:
			case DAY_IN_MONTH:
			case DAY_IN_WEEK:
			case WEEK_IN_MONTH:
			case WEEK_IN_YEAR:
			case TS:
				{
				setState(135);
				funcCall();
				}
				break;
			case MAX:
			case MIN:
			case SUM:
			case AVERAGE:
			case COUNT:
			case COUNT_DISTINCT:
			case RANGE:
			case ANY:
				{
				setState(136);
				aggregationExpr();
				}
				break;
			case DATA:
			case NAME:
			case ALIAS:
			case THIS:
				{
				setState(137);
				fieldExpr();
				}
				break;
			case T__2:
			case SELECT:
				{
				setState(138);
				subquery();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(141);
			match(T__1);
			setState(142);
			fieldPath();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectPolymorphismFieldsExprContext extends ParserRuleContext {
		public TerminalNode TYPE_OF() { return getToken(suqlParser.TYPE_OF, 0); }
		public TerminalNode END() { return getToken(suqlParser.END, 0); }
		public TerminalNode THIS() { return getToken(suqlParser.THIS, 0); }
		public FieldExprContext fieldExpr() {
			return getRuleContext(FieldExprContext.class,0);
		}
		public List<TerminalNode> WHEN() { return getTokens(suqlParser.WHEN); }
		public TerminalNode WHEN(int i) {
			return getToken(suqlParser.WHEN, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> THEN() { return getTokens(suqlParser.THEN); }
		public TerminalNode THEN(int i) {
			return getToken(suqlParser.THEN, i);
		}
		public List<SelectSimpleFieldsExprContext> selectSimpleFieldsExpr() {
			return getRuleContexts(SelectSimpleFieldsExprContext.class);
		}
		public SelectSimpleFieldsExprContext selectSimpleFieldsExpr(int i) {
			return getRuleContext(SelectSimpleFieldsExprContext.class,i);
		}
		public List<TerminalNode> ELSE() { return getTokens(suqlParser.ELSE); }
		public TerminalNode ELSE(int i) {
			return getToken(suqlParser.ELSE, i);
		}
		public SelectPolymorphismFieldsExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectPolymorphismFieldsExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterSelectPolymorphismFieldsExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitSelectPolymorphismFieldsExpr(this);
		}
	}

	public final SelectPolymorphismFieldsExprContext selectPolymorphismFieldsExpr() throws RecognitionException {
		SelectPolymorphismFieldsExprContext _localctx = new SelectPolymorphismFieldsExprContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_selectPolymorphismFieldsExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(144);
			match(TYPE_OF);
			setState(147);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(145);
				match(THIS);
				}
				break;
			case 2:
				{
				setState(146);
				fieldExpr();
				}
				break;
			}
			setState(154);
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(149);
				match(WHEN);
				setState(150);
				type();
				setState(151);
				match(THEN);
				setState(152);
				selectSimpleFieldsExpr();
				}
				}
				setState(156);
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==WHEN );
			setState(162);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ELSE) {
				{
				{
				setState(158);
				match(ELSE);
				setState(159);
				selectSimpleFieldsExpr();
				}
				}
				setState(164);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(165);
			match(END);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubqueryContext extends ParserRuleContext {
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode SELECT() { return getToken(suqlParser.SELECT, 0); }
		public TerminalNode FROM() { return getToken(suqlParser.FROM, 0); }
		public List<FieldExprContext> fieldExpr() {
			return getRuleContexts(FieldExprContext.class);
		}
		public FieldExprContext fieldExpr(int i) {
			return getRuleContext(FieldExprContext.class,i);
		}
		public SelectFieldsExprContext selectFieldsExpr() {
			return getRuleContext(SelectFieldsExprContext.class,0);
		}
		public TerminalNode WHERE() { return getToken(suqlParser.WHERE, 0); }
		public LogicalExprContext logicalExpr() {
			return getRuleContext(LogicalExprContext.class,0);
		}
		public SubqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterSubquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitSubquery(this);
		}
	}

	public final SubqueryContext subquery() throws RecognitionException {
		SubqueryContext _localctx = new SubqueryContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_subquery);
		try {
			setState(182);
			switch (_input.LA(1)) {
			case T__2:
				enterOuterAlt(_localctx, 1);
				{
				setState(167);
				match(T__2);
				setState(168);
				subquery();
				setState(169);
				match(T__3);
				}
				break;
			case SELECT:
				enterOuterAlt(_localctx, 2);
				{
				setState(171);
				match(SELECT);
				setState(174);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
				case 1:
					{
					setState(172);
					selectFieldsExpr();
					}
					break;
				case 2:
					{
					setState(173);
					fieldExpr();
					}
					break;
				}
				setState(176);
				match(FROM);
				setState(177);
				fieldExpr();
				setState(180);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
				case 1:
					{
					setState(178);
					match(WHERE);
					setState(179);
					logicalExpr(0);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalExprContext extends ParserRuleContext {
		public List<LogicalExprContext> logicalExpr() {
			return getRuleContexts(LogicalExprContext.class);
		}
		public LogicalExprContext logicalExpr(int i) {
			return getRuleContext(LogicalExprContext.class,i);
		}
		public TerminalNode NOT() { return getToken(suqlParser.NOT, 0); }
		public ComparisonExprContext comparisonExpr() {
			return getRuleContext(ComparisonExprContext.class,0);
		}
		public TerminalNode AND() { return getToken(suqlParser.AND, 0); }
		public TerminalNode OR() { return getToken(suqlParser.OR, 0); }
		public LogicalExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterLogicalExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitLogicalExpr(this);
		}
	}

	public final LogicalExprContext logicalExpr() throws RecognitionException {
		return logicalExpr(0);
	}

	private LogicalExprContext logicalExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		LogicalExprContext _localctx = new LogicalExprContext(_ctx, _parentState);
		LogicalExprContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_logicalExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(186);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(185);
					match(NOT);
					}
				}

				setState(188);
				match(T__2);
				setState(189);
				logicalExpr(0);
				setState(190);
				match(T__3);
				}
				break;
			case 2:
				{
				setState(193);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(192);
					match(NOT);
					}
				}

				setState(195);
				comparisonExpr();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(206);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(204);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
					case 1:
						{
						_localctx = new LogicalExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_logicalExpr);
						setState(198);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(199);
						match(AND);
						setState(200);
						logicalExpr(5);
						}
						break;
					case 2:
						{
						_localctx = new LogicalExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_logicalExpr);
						setState(201);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(202);
						match(OR);
						setState(203);
						logicalExpr(4);
						}
						break;
					}
					}
				}
				setState(208);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ComparisonExprContext extends ParserRuleContext {
		public FilterFieldContext filterField() {
			return getRuleContext(FilterFieldContext.class,0);
		}
		public TerminalNode IN() { return getToken(suqlParser.IN, 0); }
		public TerminalNode NOT_IN() { return getToken(suqlParser.NOT_IN, 0); }
		public TerminalNode INCLUDE_ANY() { return getToken(suqlParser.INCLUDE_ANY, 0); }
		public TerminalNode INCLUDE_ALL() { return getToken(suqlParser.INCLUDE_ALL, 0); }
		public TerminalNode EXCLUDE_ALL() { return getToken(suqlParser.EXCLUDE_ALL, 0); }
		public TerminalNode EXCLUDE_ANY() { return getToken(suqlParser.EXCLUDE_ANY, 0); }
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public ValueArrayExprContext valueArrayExpr() {
			return getRuleContext(ValueArrayExprContext.class,0);
		}
		public ValueExprContext valueExpr() {
			return getRuleContext(ValueExprContext.class,0);
		}
		public TerminalNode EQ() { return getToken(suqlParser.EQ, 0); }
		public TerminalNode NQ() { return getToken(suqlParser.NQ, 0); }
		public TerminalNode GT() { return getToken(suqlParser.GT, 0); }
		public TerminalNode LT() { return getToken(suqlParser.LT, 0); }
		public TerminalNode GTE() { return getToken(suqlParser.GTE, 0); }
		public TerminalNode LTE() { return getToken(suqlParser.LTE, 0); }
		public TerminalNode LIKE() { return getToken(suqlParser.LIKE, 0); }
		public TerminalNode INCLUDE() { return getToken(suqlParser.INCLUDE, 0); }
		public TerminalNode EXCLUDE() { return getToken(suqlParser.EXCLUDE, 0); }
		public TerminalNode CHILD_OF() { return getToken(suqlParser.CHILD_OF, 0); }
		public TerminalNode PARENT_OF() { return getToken(suqlParser.PARENT_OF, 0); }
		public TerminalNode ANCESTOR_OF() { return getToken(suqlParser.ANCESTOR_OF, 0); }
		public TerminalNode DESCENDANT_OF() { return getToken(suqlParser.DESCENDANT_OF, 0); }
		public TerminalNode IS_LEAF() { return getToken(suqlParser.IS_LEAF, 0); }
		public TerminalNode IS_ROOT() { return getToken(suqlParser.IS_ROOT, 0); }
		public TerminalNode IS_NULL() { return getToken(suqlParser.IS_NULL, 0); }
		public TerminalNode IS_NOT_NULL() { return getToken(suqlParser.IS_NOT_NULL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IS() { return getToken(suqlParser.IS, 0); }
		public TerminalNode IS_NOT() { return getToken(suqlParser.IS_NOT, 0); }
		public ComparisonExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparisonExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterComparisonExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitComparisonExpr(this);
		}
	}

	public final ComparisonExprContext comparisonExpr() throws RecognitionException {
		ComparisonExprContext _localctx = new ComparisonExprContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_comparisonExpr);
		int _la;
		try {
			setState(229);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(209);
				filterField();
				setState(210);
				_la = _input.LA(1);
				if ( !(((((_la - 62)) & ~0x3f) == 0 && ((1L << (_la - 62)) & ((1L << (IN - 62)) | (1L << (NOT_IN - 62)) | (1L << (INCLUDE_ANY - 62)) | (1L << (INCLUDE_ALL - 62)) | (1L << (EXCLUDE_ANY - 62)) | (1L << (EXCLUDE_ALL - 62)))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(213);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
				case 1:
					{
					setState(211);
					subquery();
					}
					break;
				case 2:
					{
					setState(212);
					valueArrayExpr();
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(215);
				filterField();
				setState(216);
				_la = _input.LA(1);
				if ( !(((((_la - 55)) & ~0x3f) == 0 && ((1L << (_la - 55)) & ((1L << (EQ - 55)) | (1L << (NQ - 55)) | (1L << (GT - 55)) | (1L << (LT - 55)) | (1L << (GTE - 55)) | (1L << (LTE - 55)) | (1L << (LIKE - 55)) | (1L << (INCLUDE - 55)) | (1L << (EXCLUDE - 55)) | (1L << (CHILD_OF - 55)) | (1L << (PARENT_OF - 55)) | (1L << (ANCESTOR_OF - 55)) | (1L << (DESCENDANT_OF - 55)))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(217);
				valueExpr();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(219);
				filterField();
				setState(220);
				_la = _input.LA(1);
				if ( !(_la==IS_LEAF || _la==IS_ROOT) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(222);
				filterField();
				setState(223);
				_la = _input.LA(1);
				if ( !(_la==IS_NULL || _la==IS_NOT_NULL) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(225);
				filterField();
				setState(226);
				_la = _input.LA(1);
				if ( !(_la==IS || _la==IS_NOT) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(227);
				type();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilterFieldContext extends ParserRuleContext {
		public AggregationExprContext aggregationExpr() {
			return getRuleContext(AggregationExprContext.class,0);
		}
		public FieldExprContext fieldExpr() {
			return getRuleContext(FieldExprContext.class,0);
		}
		public TerminalNode THIS() { return getToken(suqlParser.THIS, 0); }
		public FilterFieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filterField; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterFilterField(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitFilterField(this);
		}
	}

	public final FilterFieldContext filterField() throws RecognitionException {
		FilterFieldContext _localctx = new FilterFieldContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_filterField);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				{
				setState(231);
				aggregationExpr();
				}
				break;
			case 2:
				{
				setState(232);
				fieldExpr();
				}
				break;
			case 3:
				{
				setState(233);
				match(THIS);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupbyExpressionContext extends ParserRuleContext {
		public TerminalNode GROUP_BY() { return getToken(suqlParser.GROUP_BY, 0); }
		public List<FieldExprContext> fieldExpr() {
			return getRuleContexts(FieldExprContext.class);
		}
		public FieldExprContext fieldExpr(int i) {
			return getRuleContext(FieldExprContext.class,i);
		}
		public TerminalNode ROLLUP() { return getToken(suqlParser.ROLLUP, 0); }
		public TerminalNode CUBE() { return getToken(suqlParser.CUBE, 0); }
		public TerminalNode HAVING() { return getToken(suqlParser.HAVING, 0); }
		public LogicalExprContext logicalExpr() {
			return getRuleContext(LogicalExprContext.class,0);
		}
		public GroupbyExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupbyExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterGroupbyExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitGroupbyExpression(this);
		}
	}

	public final GroupbyExpressionContext groupbyExpression() throws RecognitionException {
		GroupbyExpressionContext _localctx = new GroupbyExpressionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_groupbyExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(236);
			match(GROUP_BY);
			setState(257);
			switch (_input.LA(1)) {
			case DATA:
			case NAME:
			case ALIAS:
			case THIS:
				{
				setState(237);
				fieldExpr();
				setState(242);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(238);
					match(T__0);
					setState(239);
					fieldExpr();
					}
					}
					setState(244);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ROLLUP:
			case CUBE:
				{
				setState(245);
				_la = _input.LA(1);
				if ( !(_la==ROLLUP || _la==CUBE) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(246);
				match(T__2);
				setState(247);
				fieldExpr();
				setState(252);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(248);
					match(T__0);
					setState(249);
					fieldExpr();
					}
					}
					setState(254);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(255);
				match(T__3);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(261);
			_la = _input.LA(1);
			if (_la==HAVING) {
				{
				setState(259);
				match(HAVING);
				setState(260);
				logicalExpr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationExprContext extends ParserRuleContext {
		public List<AggregationFuncNameContext> aggregationFuncName() {
			return getRuleContexts(AggregationFuncNameContext.class);
		}
		public AggregationFuncNameContext aggregationFuncName(int i) {
			return getRuleContext(AggregationFuncNameContext.class,i);
		}
		public FieldExprContext fieldExpr() {
			return getRuleContext(FieldExprContext.class,0);
		}
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public AggregationExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterAggregationExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitAggregationExpr(this);
		}
	}

	public final AggregationExprContext aggregationExpr() throws RecognitionException {
		AggregationExprContext _localctx = new AggregationExprContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_aggregationExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(263);
			aggregationFuncName();
			setState(264);
			match(T__2);
			setState(274);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				{
				setState(265);
				fieldExpr();
				}
				break;
			case 2:
				{
				setState(266);
				subquery();
				}
				break;
			case 3:
				{
				setState(267);
				aggregationFuncName();
				setState(268);
				match(T__2);
				setState(269);
				fieldExpr();
				}
				break;
			case 4:
				{
				setState(271);
				subquery();
				setState(272);
				match(T__3);
				}
				break;
			}
			setState(276);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationFuncNameContext extends ParserRuleContext {
		public TerminalNode MAX() { return getToken(suqlParser.MAX, 0); }
		public TerminalNode MIN() { return getToken(suqlParser.MIN, 0); }
		public TerminalNode SUM() { return getToken(suqlParser.SUM, 0); }
		public TerminalNode AVERAGE() { return getToken(suqlParser.AVERAGE, 0); }
		public TerminalNode COUNT() { return getToken(suqlParser.COUNT, 0); }
		public TerminalNode COUNT_DISTINCT() { return getToken(suqlParser.COUNT_DISTINCT, 0); }
		public TerminalNode RANGE() { return getToken(suqlParser.RANGE, 0); }
		public TerminalNode ANY() { return getToken(suqlParser.ANY, 0); }
		public AggregationFuncNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationFuncName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterAggregationFuncName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitAggregationFuncName(this);
		}
	}

	public final AggregationFuncNameContext aggregationFuncName() throws RecognitionException {
		AggregationFuncNameContext _localctx = new AggregationFuncNameContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_aggregationFuncName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(278);
			_la = _input.LA(1);
			if ( !(((((_la - 84)) & ~0x3f) == 0 && ((1L << (_la - 84)) & ((1L << (MAX - 84)) | (1L << (MIN - 84)) | (1L << (SUM - 84)) | (1L << (AVERAGE - 84)) | (1L << (COUNT - 84)) | (1L << (COUNT_DISTINCT - 84)) | (1L << (RANGE - 84)) | (1L << (ANY - 84)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncCallContext extends ParserRuleContext {
		public FuncNameContext funcName() {
			return getRuleContext(FuncNameContext.class,0);
		}
		public FieldExprContext fieldExpr() {
			return getRuleContext(FieldExprContext.class,0);
		}
		public List<ValueExprContext> valueExpr() {
			return getRuleContexts(ValueExprContext.class);
		}
		public ValueExprContext valueExpr(int i) {
			return getRuleContext(ValueExprContext.class,i);
		}
		public FuncCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterFuncCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitFuncCall(this);
		}
	}

	public final FuncCallContext funcCall() throws RecognitionException {
		FuncCallContext _localctx = new FuncCallContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_funcCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(280);
			funcName();
			setState(281);
			match(T__2);
			setState(284);
			switch (_input.LA(1)) {
			case DATA:
			case NAME:
			case ALIAS:
			case THIS:
				{
				setState(282);
				fieldExpr();
				}
				break;
			case NULL:
			case TREE:
			case FALSE:
			case YESTERDAY:
			case TODAY:
			case TOMORROW:
			case LAST_WEEK:
			case THIS_WEEK:
			case NEXT_WEEK:
			case LAST_MONTH:
			case THIS_MONTH:
			case NEXT_MONTH:
			case LAST_90_DAYS:
			case NEXT_90_DAYS:
			case LAST_N_DAYS:
			case NEXT_N_DAYS:
			case LAST_N_WEEKS:
			case NEXT_N_WEEKS:
			case LAST_N_MONTHS:
			case NEXT_N_MONTHS:
			case NEXT_QUARTER:
			case THIS_QUARTER:
			case LAST_QUARTER:
			case THIS_YEAR:
			case LAST_YEAR:
			case NEXT_YEAR:
			case LAST_N_QUARTERS:
			case NEXT_N_QUARTERS:
			case LAST_N_YEARS:
			case NEXT_N_YEARS:
			case THIS_FISCAL_QUARTER:
			case LAST_FISCAL_QUARTER:
			case NEXT_FISCAL_QUARTER:
			case THIS_FISCAL_YEAR:
			case LAST_FISCAL_YEAR:
			case NEXT_FISCAL_YEAR:
			case LAST_N_FISCAL_QUARTERS:
			case NEXT_N_FISCAL_QUARTERS:
			case LAST_N_FISCAL_YEARS:
			case NEXT_N_FISCAL_YEARS:
			case INTEGER_LITERAL:
			case NUMBER_LITERAL:
			case STRING_LITERAL:
			case TIME_LITERAL:
				{
				setState(283);
				valueExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(290);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(286);
				match(T__0);
				setState(287);
				valueExpr();
				}
				}
				setState(292);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(293);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncNameContext extends ParserRuleContext {
		public TerminalNode TO_LABEL() { return getToken(suqlParser.TO_LABEL, 0); }
		public TerminalNode CONVERT_CURRENCY() { return getToken(suqlParser.CONVERT_CURRENCY, 0); }
		public TerminalNode CURRENCY_TYPE() { return getToken(suqlParser.CURRENCY_TYPE, 0); }
		public TerminalNode CURRENCY_VALUE() { return getToken(suqlParser.CURRENCY_VALUE, 0); }
		public TerminalNode CONVERT_TZ() { return getToken(suqlParser.CONVERT_TZ, 0); }
		public TerminalNode CALENDRA_MONTH() { return getToken(suqlParser.CALENDRA_MONTH, 0); }
		public TerminalNode CALENDRA_QUARTER() { return getToken(suqlParser.CALENDRA_QUARTER, 0); }
		public TerminalNode CALENDRA_YEAR() { return getToken(suqlParser.CALENDRA_YEAR, 0); }
		public TerminalNode FISCAL_MONTH() { return getToken(suqlParser.FISCAL_MONTH, 0); }
		public TerminalNode FISCAL_QUARTER() { return getToken(suqlParser.FISCAL_QUARTER, 0); }
		public TerminalNode FISCAL_YEAR() { return getToken(suqlParser.FISCAL_YEAR, 0); }
		public TerminalNode DAY_IN_MONTH() { return getToken(suqlParser.DAY_IN_MONTH, 0); }
		public TerminalNode DAY_IN_WEEK() { return getToken(suqlParser.DAY_IN_WEEK, 0); }
		public TerminalNode WEEK_IN_MONTH() { return getToken(suqlParser.WEEK_IN_MONTH, 0); }
		public TerminalNode WEEK_IN_YEAR() { return getToken(suqlParser.WEEK_IN_YEAR, 0); }
		public TerminalNode TS() { return getToken(suqlParser.TS, 0); }
		public TerminalNode TZ() { return getToken(suqlParser.TZ, 0); }
		public TerminalNode GROUPING() { return getToken(suqlParser.GROUPING, 0); }
		public FuncNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterFuncName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitFuncName(this);
		}
	}

	public final FuncNameContext funcName() throws RecognitionException {
		FuncNameContext _localctx = new FuncNameContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_funcName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CURRENCY_TYPE) | (1L << CURRENCY_VALUE) | (1L << TZ) | (1L << GROUPING))) != 0) || ((((_la - 92)) & ~0x3f) == 0 && ((1L << (_la - 92)) & ((1L << (TO_LABEL - 92)) | (1L << (CONVERT_CURRENCY - 92)) | (1L << (CONVERT_TZ - 92)) | (1L << (CALENDRA_MONTH - 92)) | (1L << (CALENDRA_QUARTER - 92)) | (1L << (CALENDRA_YEAR - 92)) | (1L << (FISCAL_MONTH - 92)) | (1L << (FISCAL_QUARTER - 92)) | (1L << (FISCAL_YEAR - 92)) | (1L << (DAY_IN_MONTH - 92)) | (1L << (DAY_IN_WEEK - 92)) | (1L << (WEEK_IN_MONTH - 92)) | (1L << (WEEK_IN_YEAR - 92)) | (1L << (TS - 92)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldExprContext extends ParserRuleContext {
		public FieldPathContext fieldPath() {
			return getRuleContext(FieldPathContext.class,0);
		}
		public TerminalNode THIS() { return getToken(suqlParser.THIS, 0); }
		public ObjectTypeRefContext objectTypeRef() {
			return getRuleContext(ObjectTypeRefContext.class,0);
		}
		public FieldExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterFieldExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitFieldExpr(this);
		}
	}

	public final FieldExprContext fieldExpr() throws RecognitionException {
		FieldExprContext _localctx = new FieldExprContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_fieldExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				{
				setState(299);
				switch (_input.LA(1)) {
				case THIS:
					{
					setState(297);
					match(THIS);
					}
					break;
				case DATA:
				case NAME:
				case ALIAS:
					{
					setState(298);
					objectTypeRef();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(301);
				match(T__4);
				}
				break;
			}
			setState(304);
			fieldPath();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldPathContext extends ParserRuleContext {
		public List<FieldNameContext> fieldName() {
			return getRuleContexts(FieldNameContext.class);
		}
		public FieldNameContext fieldName(int i) {
			return getRuleContext(FieldNameContext.class,i);
		}
		public FieldPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterFieldPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitFieldPath(this);
		}
	}

	public final FieldPathContext fieldPath() throws RecognitionException {
		FieldPathContext _localctx = new FieldPathContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_fieldPath);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(306);
			fieldName();
			setState(311);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(307);
					match(T__5);
					setState(308);
					fieldName();
					}
					} 
				}
				setState(313);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldNameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(suqlParser.NAME, 0); }
		public FieldNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterFieldName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitFieldName(this);
		}
	}

	public final FieldNameContext fieldName() throws RecognitionException {
		FieldNameContext _localctx = new FieldNameContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_fieldName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(314);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectTypeRefContext extends ParserRuleContext {
		public ObjectTypeFqnContext objectTypeFqn() {
			return getRuleContext(ObjectTypeFqnContext.class,0);
		}
		public ObjectTypeNameContext objectTypeName() {
			return getRuleContext(ObjectTypeNameContext.class,0);
		}
		public TerminalNode ALIAS() { return getToken(suqlParser.ALIAS, 0); }
		public ObjectTypeRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectTypeRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterObjectTypeRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitObjectTypeRef(this);
		}
	}

	public final ObjectTypeRefContext objectTypeRef() throws RecognitionException {
		ObjectTypeRefContext _localctx = new ObjectTypeRefContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_objectTypeRef);
		try {
			setState(319);
			switch (_input.LA(1)) {
			case DATA:
				enterOuterAlt(_localctx, 1);
				{
				setState(316);
				objectTypeFqn();
				}
				break;
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(317);
				objectTypeName();
				}
				break;
			case ALIAS:
				enterOuterAlt(_localctx, 3);
				{
				setState(318);
				match(ALIAS);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectTypeFqnContext extends ParserRuleContext {
		public ObjectTypeNameContext objectTypeName() {
			return getRuleContext(ObjectTypeNameContext.class,0);
		}
		public List<ObjectRealmNameContext> objectRealmName() {
			return getRuleContexts(ObjectRealmNameContext.class);
		}
		public ObjectRealmNameContext objectRealmName(int i) {
			return getRuleContext(ObjectRealmNameContext.class,i);
		}
		public ObjectTypeFqnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectTypeFqn; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterObjectTypeFqn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitObjectTypeFqn(this);
		}
	}

	public final ObjectTypeFqnContext objectTypeFqn() throws RecognitionException {
		ObjectTypeFqnContext _localctx = new ObjectTypeFqnContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_objectTypeFqn);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(322); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(321);
				objectRealmName();
				}
				}
				setState(324); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DATA );
			setState(326);
			objectTypeName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectRealmNameContext extends ParserRuleContext {
		public TerminalNode DATA() { return getToken(suqlParser.DATA, 0); }
		public List<RealmContext> realm() {
			return getRuleContexts(RealmContext.class);
		}
		public RealmContext realm(int i) {
			return getRuleContext(RealmContext.class,i);
		}
		public ObjectRealmNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectRealmName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterObjectRealmName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitObjectRealmName(this);
		}
	}

	public final ObjectRealmNameContext objectRealmName() throws RecognitionException {
		ObjectRealmNameContext _localctx = new ObjectRealmNameContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_objectRealmName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(328);
			match(DATA);
			setState(331); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(329);
				match(T__4);
				setState(330);
				realm();
				}
				}
				setState(333); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__4 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealmContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(suqlParser.NAME, 0); }
		public RealmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterRealm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitRealm(this);
		}
	}

	public final RealmContext realm() throws RecognitionException {
		RealmContext _localctx = new RealmContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_realm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectTypeNameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(suqlParser.NAME, 0); }
		public ObjectTypeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectTypeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterObjectTypeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitObjectTypeName(this);
		}
	}

	public final ObjectTypeNameContext objectTypeName() throws RecognitionException {
		ObjectTypeNameContext _localctx = new ObjectTypeNameContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_objectTypeName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(337);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueArrayExprContext extends ParserRuleContext {
		public List<ValueExprContext> valueExpr() {
			return getRuleContexts(ValueExprContext.class);
		}
		public ValueExprContext valueExpr(int i) {
			return getRuleContext(ValueExprContext.class,i);
		}
		public ValueArrayExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueArrayExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterValueArrayExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitValueArrayExpr(this);
		}
	}

	public final ValueArrayExprContext valueArrayExpr() throws RecognitionException {
		ValueArrayExprContext _localctx = new ValueArrayExprContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_valueArrayExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			match(T__2);
			setState(348);
			_la = _input.LA(1);
			if (_la==NULL || _la==TREE || ((((_la - 107)) & ~0x3f) == 0 && ((1L << (_la - 107)) & ((1L << (FALSE - 107)) | (1L << (YESTERDAY - 107)) | (1L << (TODAY - 107)) | (1L << (TOMORROW - 107)) | (1L << (LAST_WEEK - 107)) | (1L << (THIS_WEEK - 107)) | (1L << (NEXT_WEEK - 107)) | (1L << (LAST_MONTH - 107)) | (1L << (THIS_MONTH - 107)) | (1L << (NEXT_MONTH - 107)) | (1L << (LAST_90_DAYS - 107)) | (1L << (NEXT_90_DAYS - 107)) | (1L << (LAST_N_DAYS - 107)) | (1L << (NEXT_N_DAYS - 107)) | (1L << (LAST_N_WEEKS - 107)) | (1L << (NEXT_N_WEEKS - 107)) | (1L << (LAST_N_MONTHS - 107)) | (1L << (NEXT_N_MONTHS - 107)) | (1L << (NEXT_QUARTER - 107)) | (1L << (THIS_QUARTER - 107)) | (1L << (LAST_QUARTER - 107)) | (1L << (THIS_YEAR - 107)) | (1L << (LAST_YEAR - 107)) | (1L << (NEXT_YEAR - 107)) | (1L << (LAST_N_QUARTERS - 107)) | (1L << (NEXT_N_QUARTERS - 107)) | (1L << (LAST_N_YEARS - 107)) | (1L << (NEXT_N_YEARS - 107)) | (1L << (THIS_FISCAL_QUARTER - 107)) | (1L << (LAST_FISCAL_QUARTER - 107)) | (1L << (NEXT_FISCAL_QUARTER - 107)) | (1L << (THIS_FISCAL_YEAR - 107)) | (1L << (LAST_FISCAL_YEAR - 107)) | (1L << (NEXT_FISCAL_YEAR - 107)) | (1L << (LAST_N_FISCAL_QUARTERS - 107)) | (1L << (NEXT_N_FISCAL_QUARTERS - 107)) | (1L << (LAST_N_FISCAL_YEARS - 107)) | (1L << (NEXT_N_FISCAL_YEARS - 107)) | (1L << (INTEGER_LITERAL - 107)) | (1L << (NUMBER_LITERAL - 107)) | (1L << (STRING_LITERAL - 107)) | (1L << (TIME_LITERAL - 107)))) != 0)) {
				{
				setState(340);
				valueExpr();
				setState(345);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(341);
					match(T__0);
					setState(342);
					valueExpr();
					}
					}
					setState(347);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(350);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueExprContext extends ParserRuleContext {
		public TerminalNode INTEGER_LITERAL() { return getToken(suqlParser.INTEGER_LITERAL, 0); }
		public TerminalNode NUMBER_LITERAL() { return getToken(suqlParser.NUMBER_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(suqlParser.STRING_LITERAL, 0); }
		public TerminalNode TREE() { return getToken(suqlParser.TREE, 0); }
		public TerminalNode FALSE() { return getToken(suqlParser.FALSE, 0); }
		public TimeRangeLiteralContext timeRangeLiteral() {
			return getRuleContext(TimeRangeLiteralContext.class,0);
		}
		public TerminalNode TIME_LITERAL() { return getToken(suqlParser.TIME_LITERAL, 0); }
		public TerminalNode NULL() { return getToken(suqlParser.NULL, 0); }
		public ValueExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterValueExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitValueExpr(this);
		}
	}

	public final ValueExprContext valueExpr() throws RecognitionException {
		ValueExprContext _localctx = new ValueExprContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_valueExpr);
		try {
			setState(360);
			switch (_input.LA(1)) {
			case INTEGER_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(352);
				match(INTEGER_LITERAL);
				}
				break;
			case NUMBER_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(353);
				match(NUMBER_LITERAL);
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(354);
				match(STRING_LITERAL);
				}
				break;
			case TREE:
				enterOuterAlt(_localctx, 4);
				{
				setState(355);
				match(TREE);
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 5);
				{
				setState(356);
				match(FALSE);
				}
				break;
			case YESTERDAY:
			case TODAY:
			case TOMORROW:
			case LAST_WEEK:
			case THIS_WEEK:
			case NEXT_WEEK:
			case LAST_MONTH:
			case THIS_MONTH:
			case NEXT_MONTH:
			case LAST_90_DAYS:
			case NEXT_90_DAYS:
			case LAST_N_DAYS:
			case NEXT_N_DAYS:
			case LAST_N_WEEKS:
			case NEXT_N_WEEKS:
			case LAST_N_MONTHS:
			case NEXT_N_MONTHS:
			case NEXT_QUARTER:
			case THIS_QUARTER:
			case LAST_QUARTER:
			case THIS_YEAR:
			case LAST_YEAR:
			case NEXT_YEAR:
			case LAST_N_QUARTERS:
			case NEXT_N_QUARTERS:
			case LAST_N_YEARS:
			case NEXT_N_YEARS:
			case THIS_FISCAL_QUARTER:
			case LAST_FISCAL_QUARTER:
			case NEXT_FISCAL_QUARTER:
			case THIS_FISCAL_YEAR:
			case LAST_FISCAL_YEAR:
			case NEXT_FISCAL_YEAR:
			case LAST_N_FISCAL_QUARTERS:
			case NEXT_N_FISCAL_QUARTERS:
			case LAST_N_FISCAL_YEARS:
			case NEXT_N_FISCAL_YEARS:
				enterOuterAlt(_localctx, 6);
				{
				setState(357);
				timeRangeLiteral();
				}
				break;
			case TIME_LITERAL:
				enterOuterAlt(_localctx, 7);
				{
				setState(358);
				match(TIME_LITERAL);
				}
				break;
			case NULL:
				enterOuterAlt(_localctx, 8);
				{
				setState(359);
				match(NULL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public ScaleTypeContext scaleType() {
			return getRuleContext(ScaleTypeContext.class,0);
		}
		public ComplexTypeContext complexType() {
			return getRuleContext(ComplexTypeContext.class,0);
		}
		public ObjectTypeRefContext objectTypeRef() {
			return getRuleContext(ObjectTypeRefContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_type);
		try {
			setState(365);
			switch (_input.LA(1)) {
			case ID:
			case INTEGER:
			case NUMBER:
			case STRING:
			case BOOLEAN:
			case LABEL:
			case PHONE_NO:
			case EMAIL:
			case CURRENCY:
			case CURRENCY_TYPE:
			case TIME:
			case TIME_RANGE:
			case TZ:
			case URL:
			case ENUM:
				enterOuterAlt(_localctx, 1);
				{
				setState(362);
				scaleType();
				}
				break;
			case OBJECT:
			case TREE:
			case INTERFACE:
			case ARRAY:
			case UNION:
				enterOuterAlt(_localctx, 2);
				{
				setState(363);
				complexType();
				}
				break;
			case DATA:
			case NAME:
			case ALIAS:
				enterOuterAlt(_localctx, 3);
				{
				setState(364);
				objectTypeRef();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScaleTypeContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(suqlParser.ID, 0); }
		public TerminalNode INTEGER() { return getToken(suqlParser.INTEGER, 0); }
		public TerminalNode NUMBER() { return getToken(suqlParser.NUMBER, 0); }
		public TerminalNode STRING() { return getToken(suqlParser.STRING, 0); }
		public TerminalNode BOOLEAN() { return getToken(suqlParser.BOOLEAN, 0); }
		public TerminalNode LABEL() { return getToken(suqlParser.LABEL, 0); }
		public TerminalNode PHONE_NO() { return getToken(suqlParser.PHONE_NO, 0); }
		public TerminalNode EMAIL() { return getToken(suqlParser.EMAIL, 0); }
		public TerminalNode CURRENCY() { return getToken(suqlParser.CURRENCY, 0); }
		public TerminalNode CURRENCY_TYPE() { return getToken(suqlParser.CURRENCY_TYPE, 0); }
		public TerminalNode TIME() { return getToken(suqlParser.TIME, 0); }
		public TerminalNode TIME_RANGE() { return getToken(suqlParser.TIME_RANGE, 0); }
		public TerminalNode TZ() { return getToken(suqlParser.TZ, 0); }
		public TerminalNode URL() { return getToken(suqlParser.URL, 0); }
		public TerminalNode ENUM() { return getToken(suqlParser.ENUM, 0); }
		public ScaleTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scaleType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterScaleType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitScaleType(this);
		}
	}

	public final ScaleTypeContext scaleType() throws RecognitionException {
		ScaleTypeContext _localctx = new ScaleTypeContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_scaleType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(367);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ID) | (1L << INTEGER) | (1L << NUMBER) | (1L << STRING) | (1L << BOOLEAN) | (1L << LABEL) | (1L << PHONE_NO) | (1L << EMAIL) | (1L << CURRENCY) | (1L << CURRENCY_TYPE) | (1L << TIME) | (1L << TIME_RANGE) | (1L << TZ) | (1L << URL) | (1L << ENUM))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComplexTypeContext extends ParserRuleContext {
		public TerminalNode INTERFACE() { return getToken(suqlParser.INTERFACE, 0); }
		public TerminalNode OBJECT() { return getToken(suqlParser.OBJECT, 0); }
		public TerminalNode ARRAY() { return getToken(suqlParser.ARRAY, 0); }
		public TerminalNode UNION() { return getToken(suqlParser.UNION, 0); }
		public TerminalNode TREE() { return getToken(suqlParser.TREE, 0); }
		public ComplexTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_complexType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterComplexType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitComplexType(this);
		}
	}

	public final ComplexTypeContext complexType() throws RecognitionException {
		ComplexTypeContext _localctx = new ComplexTypeContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_complexType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(369);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OBJECT) | (1L << TREE) | (1L << INTERFACE) | (1L << ARRAY) | (1L << UNION))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeRangeLiteralContext extends ParserRuleContext {
		public TerminalNode YESTERDAY() { return getToken(suqlParser.YESTERDAY, 0); }
		public TerminalNode TODAY() { return getToken(suqlParser.TODAY, 0); }
		public TerminalNode TOMORROW() { return getToken(suqlParser.TOMORROW, 0); }
		public TerminalNode LAST_WEEK() { return getToken(suqlParser.LAST_WEEK, 0); }
		public TerminalNode NEXT_WEEK() { return getToken(suqlParser.NEXT_WEEK, 0); }
		public TerminalNode THIS_WEEK() { return getToken(suqlParser.THIS_WEEK, 0); }
		public TerminalNode THIS_MONTH() { return getToken(suqlParser.THIS_MONTH, 0); }
		public TerminalNode LAST_MONTH() { return getToken(suqlParser.LAST_MONTH, 0); }
		public TerminalNode NEXT_MONTH() { return getToken(suqlParser.NEXT_MONTH, 0); }
		public TerminalNode LAST_90_DAYS() { return getToken(suqlParser.LAST_90_DAYS, 0); }
		public TerminalNode NEXT_90_DAYS() { return getToken(suqlParser.NEXT_90_DAYS, 0); }
		public TerminalNode LAST_N_DAYS() { return getToken(suqlParser.LAST_N_DAYS, 0); }
		public TerminalNode INTEGER_LITERAL() { return getToken(suqlParser.INTEGER_LITERAL, 0); }
		public TerminalNode NEXT_N_DAYS() { return getToken(suqlParser.NEXT_N_DAYS, 0); }
		public TerminalNode LAST_N_WEEKS() { return getToken(suqlParser.LAST_N_WEEKS, 0); }
		public TerminalNode NEXT_N_WEEKS() { return getToken(suqlParser.NEXT_N_WEEKS, 0); }
		public TerminalNode LAST_N_MONTHS() { return getToken(suqlParser.LAST_N_MONTHS, 0); }
		public TerminalNode NEXT_N_MONTHS() { return getToken(suqlParser.NEXT_N_MONTHS, 0); }
		public TerminalNode NEXT_QUARTER() { return getToken(suqlParser.NEXT_QUARTER, 0); }
		public TerminalNode THIS_QUARTER() { return getToken(suqlParser.THIS_QUARTER, 0); }
		public TerminalNode LAST_QUARTER() { return getToken(suqlParser.LAST_QUARTER, 0); }
		public TerminalNode NEXT_YEAR() { return getToken(suqlParser.NEXT_YEAR, 0); }
		public TerminalNode THIS_YEAR() { return getToken(suqlParser.THIS_YEAR, 0); }
		public TerminalNode LAST_YEAR() { return getToken(suqlParser.LAST_YEAR, 0); }
		public TerminalNode NEXT_N_QUARTERS() { return getToken(suqlParser.NEXT_N_QUARTERS, 0); }
		public TerminalNode LAST_N_QUARTERS() { return getToken(suqlParser.LAST_N_QUARTERS, 0); }
		public TerminalNode NEXT_N_YEARS() { return getToken(suqlParser.NEXT_N_YEARS, 0); }
		public TerminalNode LAST_N_YEARS() { return getToken(suqlParser.LAST_N_YEARS, 0); }
		public TerminalNode THIS_FISCAL_QUARTER() { return getToken(suqlParser.THIS_FISCAL_QUARTER, 0); }
		public TerminalNode LAST_FISCAL_QUARTER() { return getToken(suqlParser.LAST_FISCAL_QUARTER, 0); }
		public TerminalNode NEXT_FISCAL_QUARTER() { return getToken(suqlParser.NEXT_FISCAL_QUARTER, 0); }
		public TerminalNode THIS_FISCAL_YEAR() { return getToken(suqlParser.THIS_FISCAL_YEAR, 0); }
		public TerminalNode LAST_FISCAL_YEAR() { return getToken(suqlParser.LAST_FISCAL_YEAR, 0); }
		public TerminalNode NEXT_FISCAL_YEAR() { return getToken(suqlParser.NEXT_FISCAL_YEAR, 0); }
		public TerminalNode LAST_N_FISCAL_QUARTERS() { return getToken(suqlParser.LAST_N_FISCAL_QUARTERS, 0); }
		public TerminalNode NEXT_N_FISCAL_QUARTERS() { return getToken(suqlParser.NEXT_N_FISCAL_QUARTERS, 0); }
		public TerminalNode LAST_N_FISCAL_YEARS() { return getToken(suqlParser.LAST_N_FISCAL_YEARS, 0); }
		public TerminalNode NEXT_N_FISCAL_YEARS() { return getToken(suqlParser.NEXT_N_FISCAL_YEARS, 0); }
		public TimeRangeLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeRangeLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).enterTimeRangeLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof suqlListener ) ((suqlListener)listener).exitTimeRangeLiteral(this);
		}
	}

	public final TimeRangeLiteralContext timeRangeLiteral() throws RecognitionException {
		TimeRangeLiteralContext _localctx = new TimeRangeLiteralContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_timeRangeLiteral);
		try {
			setState(436);
			switch (_input.LA(1)) {
			case YESTERDAY:
				enterOuterAlt(_localctx, 1);
				{
				setState(371);
				match(YESTERDAY);
				}
				break;
			case TODAY:
				enterOuterAlt(_localctx, 2);
				{
				setState(372);
				match(TODAY);
				}
				break;
			case TOMORROW:
				enterOuterAlt(_localctx, 3);
				{
				setState(373);
				match(TOMORROW);
				}
				break;
			case LAST_WEEK:
				enterOuterAlt(_localctx, 4);
				{
				setState(374);
				match(LAST_WEEK);
				}
				break;
			case NEXT_WEEK:
				enterOuterAlt(_localctx, 5);
				{
				setState(375);
				match(NEXT_WEEK);
				}
				break;
			case THIS_WEEK:
				enterOuterAlt(_localctx, 6);
				{
				setState(376);
				match(THIS_WEEK);
				}
				break;
			case THIS_MONTH:
				enterOuterAlt(_localctx, 7);
				{
				setState(377);
				match(THIS_MONTH);
				}
				break;
			case LAST_MONTH:
				enterOuterAlt(_localctx, 8);
				{
				setState(378);
				match(LAST_MONTH);
				}
				break;
			case NEXT_MONTH:
				enterOuterAlt(_localctx, 9);
				{
				setState(379);
				match(NEXT_MONTH);
				}
				break;
			case LAST_90_DAYS:
				enterOuterAlt(_localctx, 10);
				{
				setState(380);
				match(LAST_90_DAYS);
				}
				break;
			case NEXT_90_DAYS:
				enterOuterAlt(_localctx, 11);
				{
				setState(381);
				match(NEXT_90_DAYS);
				}
				break;
			case LAST_N_DAYS:
				enterOuterAlt(_localctx, 12);
				{
				setState(382);
				match(LAST_N_DAYS);
				setState(383);
				match(T__1);
				setState(384);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_N_DAYS:
				enterOuterAlt(_localctx, 13);
				{
				setState(385);
				match(NEXT_N_DAYS);
				setState(386);
				match(T__1);
				setState(387);
				match(INTEGER_LITERAL);
				}
				break;
			case LAST_N_WEEKS:
				enterOuterAlt(_localctx, 14);
				{
				setState(388);
				match(LAST_N_WEEKS);
				setState(389);
				match(T__1);
				setState(390);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_N_WEEKS:
				enterOuterAlt(_localctx, 15);
				{
				setState(391);
				match(NEXT_N_WEEKS);
				setState(392);
				match(T__1);
				setState(393);
				match(INTEGER_LITERAL);
				}
				break;
			case LAST_N_MONTHS:
				enterOuterAlt(_localctx, 16);
				{
				setState(394);
				match(LAST_N_MONTHS);
				setState(395);
				match(T__1);
				setState(396);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_N_MONTHS:
				enterOuterAlt(_localctx, 17);
				{
				setState(397);
				match(NEXT_N_MONTHS);
				setState(398);
				match(T__1);
				setState(399);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_QUARTER:
				enterOuterAlt(_localctx, 18);
				{
				setState(400);
				match(NEXT_QUARTER);
				}
				break;
			case THIS_QUARTER:
				enterOuterAlt(_localctx, 19);
				{
				setState(401);
				match(THIS_QUARTER);
				}
				break;
			case LAST_QUARTER:
				enterOuterAlt(_localctx, 20);
				{
				setState(402);
				match(LAST_QUARTER);
				}
				break;
			case NEXT_YEAR:
				enterOuterAlt(_localctx, 21);
				{
				setState(403);
				match(NEXT_YEAR);
				}
				break;
			case THIS_YEAR:
				enterOuterAlt(_localctx, 22);
				{
				setState(404);
				match(THIS_YEAR);
				}
				break;
			case LAST_YEAR:
				enterOuterAlt(_localctx, 23);
				{
				setState(405);
				match(LAST_YEAR);
				}
				break;
			case NEXT_N_QUARTERS:
				enterOuterAlt(_localctx, 24);
				{
				setState(406);
				match(NEXT_N_QUARTERS);
				setState(407);
				match(T__1);
				setState(408);
				match(INTEGER_LITERAL);
				}
				break;
			case LAST_N_QUARTERS:
				enterOuterAlt(_localctx, 25);
				{
				setState(409);
				match(LAST_N_QUARTERS);
				setState(410);
				match(T__1);
				setState(411);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_N_YEARS:
				enterOuterAlt(_localctx, 26);
				{
				setState(412);
				match(NEXT_N_YEARS);
				setState(413);
				match(T__1);
				setState(414);
				match(INTEGER_LITERAL);
				}
				break;
			case LAST_N_YEARS:
				enterOuterAlt(_localctx, 27);
				{
				setState(415);
				match(LAST_N_YEARS);
				setState(416);
				match(T__1);
				setState(417);
				match(INTEGER_LITERAL);
				}
				break;
			case THIS_FISCAL_QUARTER:
				enterOuterAlt(_localctx, 28);
				{
				setState(418);
				match(THIS_FISCAL_QUARTER);
				}
				break;
			case LAST_FISCAL_QUARTER:
				enterOuterAlt(_localctx, 29);
				{
				setState(419);
				match(LAST_FISCAL_QUARTER);
				}
				break;
			case NEXT_FISCAL_QUARTER:
				enterOuterAlt(_localctx, 30);
				{
				setState(420);
				match(NEXT_FISCAL_QUARTER);
				}
				break;
			case THIS_FISCAL_YEAR:
				enterOuterAlt(_localctx, 31);
				{
				setState(421);
				match(THIS_FISCAL_YEAR);
				}
				break;
			case LAST_FISCAL_YEAR:
				enterOuterAlt(_localctx, 32);
				{
				setState(422);
				match(LAST_FISCAL_YEAR);
				}
				break;
			case NEXT_FISCAL_YEAR:
				enterOuterAlt(_localctx, 33);
				{
				setState(423);
				match(NEXT_FISCAL_YEAR);
				}
				break;
			case LAST_N_FISCAL_QUARTERS:
				enterOuterAlt(_localctx, 34);
				{
				setState(424);
				match(LAST_N_FISCAL_QUARTERS);
				setState(425);
				match(T__1);
				setState(426);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_N_FISCAL_QUARTERS:
				enterOuterAlt(_localctx, 35);
				{
				setState(427);
				match(NEXT_N_FISCAL_QUARTERS);
				setState(428);
				match(T__1);
				setState(429);
				match(INTEGER_LITERAL);
				}
				break;
			case LAST_N_FISCAL_YEARS:
				enterOuterAlt(_localctx, 36);
				{
				setState(430);
				match(LAST_N_FISCAL_YEARS);
				setState(431);
				match(T__1);
				setState(432);
				match(INTEGER_LITERAL);
				}
				break;
			case NEXT_N_FISCAL_YEARS:
				enterOuterAlt(_localctx, 37);
				{
				setState(433);
				match(NEXT_N_FISCAL_YEARS);
				setState(434);
				match(T__1);
				setState(435);
				match(INTEGER_LITERAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 6:
			return logicalExpr_sempred((LogicalExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean logicalExpr_sempred(LogicalExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\u0099\u01b9\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\3\2\3\2\3\2\3\2\7\2?\n\2\f\2"+
		"\16\2B\13\2\5\2D\n\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2L\n\2\3\2\3\2\5\2P\n\2"+
		"\3\2\5\2S\n\2\3\2\3\2\3\2\3\2\7\2Y\n\2\f\2\16\2\\\13\2\3\2\5\2_\n\2\3"+
		"\2\3\2\5\2c\n\2\5\2e\n\2\3\2\3\2\5\2i\n\2\3\2\3\2\5\2m\n\2\3\3\3\3\3\3"+
		"\7\3r\n\3\f\3\16\3u\13\3\3\3\3\3\3\3\7\3z\n\3\f\3\16\3}\13\3\5\3\177\n"+
		"\3\3\4\3\4\3\4\7\4\u0084\n\4\f\4\16\4\u0087\13\4\3\5\3\5\3\5\3\5\3\5\5"+
		"\5\u008e\n\5\3\5\3\5\3\5\3\6\3\6\3\6\5\6\u0096\n\6\3\6\3\6\3\6\3\6\3\6"+
		"\6\6\u009d\n\6\r\6\16\6\u009e\3\6\3\6\7\6\u00a3\n\6\f\6\16\6\u00a6\13"+
		"\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00b1\n\7\3\7\3\7\3\7\3\7\5"+
		"\7\u00b7\n\7\5\7\u00b9\n\7\3\b\3\b\5\b\u00bd\n\b\3\b\3\b\3\b\3\b\3\b\5"+
		"\b\u00c4\n\b\3\b\5\b\u00c7\n\b\3\b\3\b\3\b\3\b\3\b\3\b\7\b\u00cf\n\b\f"+
		"\b\16\b\u00d2\13\b\3\t\3\t\3\t\3\t\5\t\u00d8\n\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00e8\n\t\3\n\3\n\3\n\5\n\u00ed"+
		"\n\n\3\13\3\13\3\13\3\13\7\13\u00f3\n\13\f\13\16\13\u00f6\13\13\3\13\3"+
		"\13\3\13\3\13\3\13\7\13\u00fd\n\13\f\13\16\13\u0100\13\13\3\13\3\13\5"+
		"\13\u0104\n\13\3\13\3\13\5\13\u0108\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\5\f\u0115\n\f\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\5\16"+
		"\u011f\n\16\3\16\3\16\7\16\u0123\n\16\f\16\16\16\u0126\13\16\3\16\3\16"+
		"\3\17\3\17\3\20\3\20\5\20\u012e\n\20\3\20\5\20\u0131\n\20\3\20\3\20\3"+
		"\21\3\21\3\21\7\21\u0138\n\21\f\21\16\21\u013b\13\21\3\22\3\22\3\23\3"+
		"\23\3\23\5\23\u0142\n\23\3\24\6\24\u0145\n\24\r\24\16\24\u0146\3\24\3"+
		"\24\3\25\3\25\3\25\6\25\u014e\n\25\r\25\16\25\u014f\3\26\3\26\3\27\3\27"+
		"\3\30\3\30\3\30\3\30\7\30\u015a\n\30\f\30\16\30\u015d\13\30\5\30\u015f"+
		"\n\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u016b\n\31"+
		"\3\32\3\32\3\32\5\32\u0170\n\32\3\33\3\33\3\34\3\34\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\5\35\u01b7\n\35\3\35\2\3\16\36\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668\2\16\3\2\20\21\3\2\22\23"+
		"\4\2@ADG\5\29?BCHK\3\2LM\3\2\33\34\3\2NO\3\2\26\27\3\2V]\6\2&\'**\62\62"+
		"^k\5\2\35&(+..\4\2,-/\61\u0200\2C\3\2\2\2\4~\3\2\2\2\6\u0080\3\2\2\2\b"+
		"\u008d\3\2\2\2\n\u0092\3\2\2\2\f\u00b8\3\2\2\2\16\u00c6\3\2\2\2\20\u00e7"+
		"\3\2\2\2\22\u00ec\3\2\2\2\24\u00ee\3\2\2\2\26\u0109\3\2\2\2\30\u0118\3"+
		"\2\2\2\32\u011a\3\2\2\2\34\u0129\3\2\2\2\36\u0130\3\2\2\2 \u0134\3\2\2"+
		"\2\"\u013c\3\2\2\2$\u0141\3\2\2\2&\u0144\3\2\2\2(\u014a\3\2\2\2*\u0151"+
		"\3\2\2\2,\u0153\3\2\2\2.\u0155\3\2\2\2\60\u016a\3\2\2\2\62\u016f\3\2\2"+
		"\2\64\u0171\3\2\2\2\66\u0173\3\2\2\28\u01b6\3\2\2\2:;\7\t\2\2;@\5(\25"+
		"\2<=\7\3\2\2=?\5(\25\2><\3\2\2\2?B\3\2\2\2@>\3\2\2\2@A\3\2\2\2AD\3\2\2"+
		"\2B@\3\2\2\2C:\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7\n\2\2FG\5\4\3\2GH\7\13"+
		"\2\2HK\5$\23\2IJ\7\63\2\2JL\7\66\2\2KI\3\2\2\2KL\3\2\2\2LO\3\2\2\2MN\7"+
		"\f\2\2NP\5\16\b\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QS\5\24\13\2RQ\3\2\2\2"+
		"RS\3\2\2\2Sd\3\2\2\2TU\7\17\2\2UZ\5\36\20\2VW\7\3\2\2WY\5\36\20\2XV\3"+
		"\2\2\2Y\\\3\2\2\2ZX\3\2\2\2Z[\3\2\2\2[^\3\2\2\2\\Z\3\2\2\2]_\t\2\2\2^"+
		"]\3\2\2\2^_\3\2\2\2_b\3\2\2\2`a\7\31\2\2ac\t\3\2\2b`\3\2\2\2bc\3\2\2\2"+
		"ce\3\2\2\2dT\3\2\2\2de\3\2\2\2eh\3\2\2\2fg\7\24\2\2gi\7\u0093\2\2hf\3"+
		"\2\2\2hi\3\2\2\2il\3\2\2\2jk\7\25\2\2km\7\u0093\2\2lj\3\2\2\2lm\3\2\2"+
		"\2m\3\3\2\2\2ns\5\6\4\2op\7\3\2\2pr\5\n\6\2qo\3\2\2\2ru\3\2\2\2sq\3\2"+
		"\2\2st\3\2\2\2t\177\3\2\2\2us\3\2\2\2v{\5\n\6\2wx\7\3\2\2xz\5\n\6\2yw"+
		"\3\2\2\2z}\3\2\2\2{y\3\2\2\2{|\3\2\2\2|\177\3\2\2\2}{\3\2\2\2~n\3\2\2"+
		"\2~v\3\2\2\2\177\5\3\2\2\2\u0080\u0085\5\b\5\2\u0081\u0082\7\3\2\2\u0082"+
		"\u0084\5\b\5\2\u0083\u0081\3\2\2\2\u0084\u0087\3\2\2\2\u0085\u0083\3\2"+
		"\2\2\u0085\u0086\3\2\2\2\u0086\7\3\2\2\2\u0087\u0085\3\2\2\2\u0088\u008e"+
		"\5\60\31\2\u0089\u008e\5\32\16\2\u008a\u008e\5\26\f\2\u008b\u008e\5\36"+
		"\20\2\u008c\u008e\5\f\7\2\u008d\u0088\3\2\2\2\u008d\u0089\3\2\2\2\u008d"+
		"\u008a\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008c\3\2\2\2\u008e\u008f\3\2"+
		"\2\2\u008f\u0090\7\4\2\2\u0090\u0091\5 \21\2\u0091\t\3\2\2\2\u0092\u0095"+
		"\7P\2\2\u0093\u0096\7U\2\2\u0094\u0096\5\36\20\2\u0095\u0093\3\2\2\2\u0095"+
		"\u0094\3\2\2\2\u0096\u009c\3\2\2\2\u0097\u0098\7Q\2\2\u0098\u0099\5\62"+
		"\32\2\u0099\u009a\7R\2\2\u009a\u009b\5\6\4\2\u009b\u009d\3\2\2\2\u009c"+
		"\u0097\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2"+
		"\2\2\u009f\u00a4\3\2\2\2\u00a0\u00a1\7S\2\2\u00a1\u00a3\5\6\4\2\u00a2"+
		"\u00a0\3\2\2\2\u00a3\u00a6\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5\3\2"+
		"\2\2\u00a5\u00a7\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a7\u00a8\7T\2\2\u00a8"+
		"\13\3\2\2\2\u00a9\u00aa\7\5\2\2\u00aa\u00ab\5\f\7\2\u00ab\u00ac\7\6\2"+
		"\2\u00ac\u00b9\3\2\2\2\u00ad\u00b0\7\n\2\2\u00ae\u00b1\5\4\3\2\u00af\u00b1"+
		"\5\36\20\2\u00b0\u00ae\3\2\2\2\u00b0\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2"+
		"\u00b2\u00b3\7\13\2\2\u00b3\u00b6\5\36\20\2\u00b4\u00b5\7\f\2\2\u00b5"+
		"\u00b7\5\16\b\2\u00b6\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3"+
		"\2\2\2\u00b8\u00a9\3\2\2\2\u00b8\u00ad\3\2\2\2\u00b9\r\3\2\2\2\u00ba\u00bc"+
		"\b\b\1\2\u00bb\u00bd\7\32\2\2\u00bc\u00bb\3\2\2\2\u00bc\u00bd\3\2\2\2"+
		"\u00bd\u00be\3\2\2\2\u00be\u00bf\7\5\2\2\u00bf\u00c0\5\16\b\2\u00c0\u00c1"+
		"\7\6\2\2\u00c1\u00c7\3\2\2\2\u00c2\u00c4\7\32\2\2\u00c3\u00c2\3\2\2\2"+
		"\u00c3\u00c4\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c7\5\20\t\2\u00c6\u00ba"+
		"\3\2\2\2\u00c6\u00c3\3\2\2\2\u00c7\u00d0\3\2\2\2\u00c8\u00c9\f\6\2\2\u00c9"+
		"\u00ca\7\67\2\2\u00ca\u00cf\5\16\b\7\u00cb\u00cc\f\5\2\2\u00cc\u00cd\7"+
		"8\2\2\u00cd\u00cf\5\16\b\6\u00ce\u00c8\3\2\2\2\u00ce\u00cb\3\2\2\2\u00cf"+
		"\u00d2\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\17\3\2\2"+
		"\2\u00d2\u00d0\3\2\2\2\u00d3\u00d4\5\22\n\2\u00d4\u00d7\t\4\2\2\u00d5"+
		"\u00d8\5\f\7\2\u00d6\u00d8\5.\30\2\u00d7\u00d5\3\2\2\2\u00d7\u00d6\3\2"+
		"\2\2\u00d8\u00e8\3\2\2\2\u00d9\u00da\5\22\n\2\u00da\u00db\t\5\2\2\u00db"+
		"\u00dc\5\60\31\2\u00dc\u00e8\3\2\2\2\u00dd\u00de\5\22\n\2\u00de\u00df"+
		"\t\6\2\2\u00df\u00e8\3\2\2\2\u00e0\u00e1\5\22\n\2\u00e1\u00e2\t\7\2\2"+
		"\u00e2\u00e8\3\2\2\2\u00e3\u00e4\5\22\n\2\u00e4\u00e5\t\b\2\2\u00e5\u00e6"+
		"\5\62\32\2\u00e6\u00e8\3\2\2\2\u00e7\u00d3\3\2\2\2\u00e7\u00d9\3\2\2\2"+
		"\u00e7\u00dd\3\2\2\2\u00e7\u00e0\3\2\2\2\u00e7\u00e3\3\2\2\2\u00e8\21"+
		"\3\2\2\2\u00e9\u00ed\5\26\f\2\u00ea\u00ed\5\36\20\2\u00eb\u00ed\7U\2\2"+
		"\u00ec\u00e9\3\2\2\2\u00ec\u00ea\3\2\2\2\u00ec\u00eb\3\2\2\2\u00ed\23"+
		"\3\2\2\2\u00ee\u0103\7\r\2\2\u00ef\u00f4\5\36\20\2\u00f0\u00f1\7\3\2\2"+
		"\u00f1\u00f3\5\36\20\2\u00f2\u00f0\3\2\2\2\u00f3\u00f6\3\2\2\2\u00f4\u00f2"+
		"\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u0104\3\2\2\2\u00f6\u00f4\3\2\2\2\u00f7"+
		"\u00f8\t\t\2\2\u00f8\u00f9\7\5\2\2\u00f9\u00fe\5\36\20\2\u00fa\u00fb\7"+
		"\3\2\2\u00fb\u00fd\5\36\20\2\u00fc\u00fa\3\2\2\2\u00fd\u0100\3\2\2\2\u00fe"+
		"\u00fc\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\u0101\3\2\2\2\u0100\u00fe\3\2"+
		"\2\2\u0101\u0102\7\6\2\2\u0102\u0104\3\2\2\2\u0103\u00ef\3\2\2\2\u0103"+
		"\u00f7\3\2\2\2\u0104\u0107\3\2\2\2\u0105\u0106\7\16\2\2\u0106\u0108\5"+
		"\16\b\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2\2\2\u0108\25\3\2\2\2\u0109"+
		"\u010a\5\30\r\2\u010a\u0114\7\5\2\2\u010b\u0115\5\36\20\2\u010c\u0115"+
		"\5\f\7\2\u010d\u010e\5\30\r\2\u010e\u010f\7\5\2\2\u010f\u0110\5\36\20"+
		"\2\u0110\u0115\3\2\2\2\u0111\u0112\5\f\7\2\u0112\u0113\7\6\2\2\u0113\u0115"+
		"\3\2\2\2\u0114\u010b\3\2\2\2\u0114\u010c\3\2\2\2\u0114\u010d\3\2\2\2\u0114"+
		"\u0111\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0117\7\6\2\2\u0117\27\3\2\2"+
		"\2\u0118\u0119\t\n\2\2\u0119\31\3\2\2\2\u011a\u011b\5\34\17\2\u011b\u011e"+
		"\7\5\2\2\u011c\u011f\5\36\20\2\u011d\u011f\5\60\31\2\u011e\u011c\3\2\2"+
		"\2\u011e\u011d\3\2\2\2\u011f\u0124\3\2\2\2\u0120\u0121\7\3\2\2\u0121\u0123"+
		"\5\60\31\2\u0122\u0120\3\2\2\2\u0123\u0126\3\2\2\2\u0124\u0122\3\2\2\2"+
		"\u0124\u0125\3\2\2\2\u0125\u0127\3\2\2\2\u0126\u0124\3\2\2\2\u0127\u0128"+
		"\7\6\2\2\u0128\33\3\2\2\2\u0129\u012a\t\13\2\2\u012a\35\3\2\2\2\u012b"+
		"\u012e\7U\2\2\u012c\u012e\5$\23\2\u012d\u012b\3\2\2\2\u012d\u012c\3\2"+
		"\2\2\u012e\u012f\3\2\2\2\u012f\u0131\7\7\2\2\u0130\u012d\3\2\2\2\u0130"+
		"\u0131\3\2\2\2\u0131\u0132\3\2\2\2\u0132\u0133\5 \21\2\u0133\37\3\2\2"+
		"\2\u0134\u0139\5\"\22\2\u0135\u0136\7\b\2\2\u0136\u0138\5\"\22\2\u0137"+
		"\u0135\3\2\2\2\u0138\u013b\3\2\2\2\u0139\u0137\3\2\2\2\u0139\u013a\3\2"+
		"\2\2\u013a!\3\2\2\2\u013b\u0139\3\2\2\2\u013c\u013d\7\65\2\2\u013d#\3"+
		"\2\2\2\u013e\u0142\5&\24\2\u013f\u0142\5,\27\2\u0140\u0142\7\66\2\2\u0141"+
		"\u013e\3\2\2\2\u0141\u013f\3\2\2\2\u0141\u0140\3\2\2\2\u0142%\3\2\2\2"+
		"\u0143\u0145\5(\25\2\u0144\u0143\3\2\2\2\u0145\u0146\3\2\2\2\u0146\u0144"+
		"\3\2\2\2\u0146\u0147\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u0149\5,\27\2\u0149"+
		"\'\3\2\2\2\u014a\u014d\7\64\2\2\u014b\u014c\7\7\2\2\u014c\u014e\5*\26"+
		"\2\u014d\u014b\3\2\2\2\u014e\u014f\3\2\2\2\u014f\u014d\3\2\2\2\u014f\u0150"+
		"\3\2\2\2\u0150)\3\2\2\2\u0151\u0152\7\65\2\2\u0152+\3\2\2\2\u0153\u0154"+
		"\7\65\2\2\u0154-\3\2\2\2\u0155\u015e\7\5\2\2\u0156\u015b\5\60\31\2\u0157"+
		"\u0158\7\3\2\2\u0158\u015a\5\60\31\2\u0159\u0157\3\2\2\2\u015a\u015d\3"+
		"\2\2\2\u015b\u0159\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015f\3\2\2\2\u015d"+
		"\u015b\3\2\2\2\u015e\u0156\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u0160\3\2"+
		"\2\2\u0160\u0161\7\6\2\2\u0161/\3\2\2\2\u0162\u016b\7\u0093\2\2\u0163"+
		"\u016b\7\u0094\2\2\u0164\u016b\7\u0095\2\2\u0165\u016b\7-\2\2\u0166\u016b"+
		"\7m\2\2\u0167\u016b\58\35\2\u0168\u016b\7\u0096\2\2\u0169\u016b\7\30\2"+
		"\2\u016a\u0162\3\2\2\2\u016a\u0163\3\2\2\2\u016a\u0164\3\2\2\2\u016a\u0165"+
		"\3\2\2\2\u016a\u0166\3\2\2\2\u016a\u0167\3\2\2\2\u016a\u0168\3\2\2\2\u016a"+
		"\u0169\3\2\2\2\u016b\61\3\2\2\2\u016c\u0170\5\64\33\2\u016d\u0170\5\66"+
		"\34\2\u016e\u0170\5$\23\2\u016f\u016c\3\2\2\2\u016f\u016d\3\2\2\2\u016f"+
		"\u016e\3\2\2\2\u0170\63\3\2\2\2\u0171\u0172\t\f\2\2\u0172\65\3\2\2\2\u0173"+
		"\u0174\t\r\2\2\u0174\67\3\2\2\2\u0175\u01b7\7n\2\2\u0176\u01b7\7o\2\2"+
		"\u0177\u01b7\7p\2\2\u0178\u01b7\7q\2\2\u0179\u01b7\7s\2\2\u017a\u01b7"+
		"\7r\2\2\u017b\u01b7\7u\2\2\u017c\u01b7\7t\2\2\u017d\u01b7\7v\2\2\u017e"+
		"\u01b7\7w\2\2\u017f\u01b7\7x\2\2\u0180\u0181\7y\2\2\u0181\u0182\7\4\2"+
		"\2\u0182\u01b7\7\u0093\2\2\u0183\u0184\7z\2\2\u0184\u0185\7\4\2\2\u0185"+
		"\u01b7\7\u0093\2\2\u0186\u0187\7{\2\2\u0187\u0188\7\4\2\2\u0188\u01b7"+
		"\7\u0093\2\2\u0189\u018a\7|\2\2\u018a\u018b\7\4\2\2\u018b\u01b7\7\u0093"+
		"\2\2\u018c\u018d\7}\2\2\u018d\u018e\7\4\2\2\u018e\u01b7\7\u0093\2\2\u018f"+
		"\u0190\7~\2\2\u0190\u0191\7\4\2\2\u0191\u01b7\7\u0093\2\2\u0192\u01b7"+
		"\7\177\2\2\u0193\u01b7\7\u0080\2\2\u0194\u01b7\7\u0081\2\2\u0195\u01b7"+
		"\7\u0084\2\2\u0196\u01b7\7\u0082\2\2\u0197\u01b7\7\u0083\2\2\u0198\u0199"+
		"\7\u0086\2\2\u0199\u019a\7\4\2\2\u019a\u01b7\7\u0093\2\2\u019b\u019c\7"+
		"\u0085\2\2\u019c\u019d\7\4\2\2\u019d\u01b7\7\u0093\2\2\u019e\u019f\7\u0088"+
		"\2\2\u019f\u01a0\7\4\2\2\u01a0\u01b7\7\u0093\2\2\u01a1\u01a2\7\u0087\2"+
		"\2\u01a2\u01a3\7\4\2\2\u01a3\u01b7\7\u0093\2\2\u01a4\u01b7\7\u0089\2\2"+
		"\u01a5\u01b7\7\u008a\2\2\u01a6\u01b7\7\u008b\2\2\u01a7\u01b7\7\u008c\2"+
		"\2\u01a8\u01b7\7\u008d\2\2\u01a9\u01b7\7\u008e\2\2\u01aa\u01ab\7\u008f"+
		"\2\2\u01ab\u01ac\7\4\2\2\u01ac\u01b7\7\u0093\2\2\u01ad\u01ae\7\u0090\2"+
		"\2\u01ae\u01af\7\4\2\2\u01af\u01b7\7\u0093\2\2\u01b0\u01b1\7\u0091\2\2"+
		"\u01b1\u01b2\7\4\2\2\u01b2\u01b7\7\u0093\2\2\u01b3\u01b4\7\u0092\2\2\u01b4"+
		"\u01b5\7\4\2\2\u01b5\u01b7\7\u0093\2\2\u01b6\u0175\3\2\2\2\u01b6\u0176"+
		"\3\2\2\2\u01b6\u0177\3\2\2\2\u01b6\u0178\3\2\2\2\u01b6\u0179\3\2\2\2\u01b6"+
		"\u017a\3\2\2\2\u01b6\u017b\3\2\2\2\u01b6\u017c\3\2\2\2\u01b6\u017d\3\2"+
		"\2\2\u01b6\u017e\3\2\2\2\u01b6\u017f\3\2\2\2\u01b6\u0180\3\2\2\2\u01b6"+
		"\u0183\3\2\2\2\u01b6\u0186\3\2\2\2\u01b6\u0189\3\2\2\2\u01b6\u018c\3\2"+
		"\2\2\u01b6\u018f\3\2\2\2\u01b6\u0192\3\2\2\2\u01b6\u0193\3\2\2\2\u01b6"+
		"\u0194\3\2\2\2\u01b6\u0195\3\2\2\2\u01b6\u0196\3\2\2\2\u01b6\u0197\3\2"+
		"\2\2\u01b6\u0198\3\2\2\2\u01b6\u019b\3\2\2\2\u01b6\u019e\3\2\2\2\u01b6"+
		"\u01a1\3\2\2\2\u01b6\u01a4\3\2\2\2\u01b6\u01a5\3\2\2\2\u01b6\u01a6\3\2"+
		"\2\2\u01b6\u01a7\3\2\2\2\u01b6\u01a8\3\2\2\2\u01b6\u01a9\3\2\2\2\u01b6"+
		"\u01aa\3\2\2\2\u01b6\u01ad\3\2\2\2\u01b6\u01b0\3\2\2\2\u01b6\u01b3\3\2"+
		"\2\2\u01b79\3\2\2\2\62@CKORZ^bdhls{~\u0085\u008d\u0095\u009e\u00a4\u00b0"+
		"\u00b6\u00b8\u00bc\u00c3\u00c6\u00ce\u00d0\u00d7\u00e7\u00ec\u00f4\u00fe"+
		"\u0103\u0107\u0114\u011e\u0124\u012d\u0130\u0139\u0141\u0146\u014f\u015b"+
		"\u015e\u016a\u016f\u01b6";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}