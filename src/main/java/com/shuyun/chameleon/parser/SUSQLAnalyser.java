package com.shuyun.chameleon.parser;

import com.shuyun.chameleon.sql.*;
import org.apache.log4j.Logger;

import java.util.Stack;

/**
 * Created by dongpengfei
 * Date 17/8/31
 * Time 下午9:31
 */

public class SUSQLAnalyser extends suqlBaseListener {
    private static Logger                   LOG							= Logger.getLogger(SUSQLAnalyser.class);
    private static final boolean			isTraceEnabled				= LOG.isTraceEnabled();

    private final Stack<SUSQLConstruct>     sqlConstructStack			= new Stack<SUSQLConstruct>();
    private final Stack<SUSQLQueryContext>	queryContextStack			= new Stack<SUSQLQueryContext>();
    private SUSQLConstruct					sql;


    public SUSQLConstruct getSUSQLConstruct() {

        return sql;
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterQuery(suqlParser.QueryContext ctx) {
        LOG.trace(">>>> enterQuery");
        queryContextStack.push(SUSQLQueryContext.SELECT);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitQuery(suqlParser.QueryContext ctx) {
        LOG.trace(">>>> exitQuery");
        SUSQLSelect susqlSelect = SUSQLBuilder.build().select().withSQLQueryContext(queryContextStack.peek());


        Stack<SUSQLConstruct> tempStack = new Stack<SUSQLConstruct>();

        SUSQLConstruct temp = null;
        while (!sqlConstructStack.isEmpty()) {
            temp = sqlConstructStack.pop();
            LOG.trace(String.format("TYPE: %s; QUERY: %s", temp.getActualType(), temp.toSql()));
            if (temp.getActualType() == SUSQLConstructType.WHERE) {
//                select.withWhereClause((SQLWhere) temp);
            } else if (temp.getActualType() == SUSQLConstructType.FROM) {
                susqlSelect.withFromClause((SUSQLFrom) temp);
            } else if (temp.getActualType() == SUSQLConstructType.HAVING) {
//                select.withHavingClause((SQLHaving) temp);
            } else if (temp.getActualType() == SUSQLConstructType.ORDER_BY) {
//                select.withOrderByClause((SQLOrderBy) temp);
            } else if (temp.getActualType() == SUSQLConstructType.GROUP_BY) {
//                select.withGroupBy((SQLGroupBy) temp);
            } else if (temp.getActualType() == SUSQLConstructType.ASTERISK) {
//                select.withSelectedItem(temp);
            } else if (temp.getActualType() == SUSQLConstructType.IGNORE_TOKEN) {
                LOG.trace(String.format("IGNORE TOKEN encountered"));
                break;
            } else if (temp.getActualType() == SUSQLConstructType.SUBSELECT
                    || temp.getActualType() == SUSQLConstructType.SELECT) {
                sqlConstructStack.push(temp);
                break;
            } else {
                tempStack.push(temp);
            }
        }

        while (!tempStack.isEmpty()) {
            if (tempStack.peek().getActualType() == SUSQLConstructType.SELECT
                    || tempStack.peek().getActualType() == SUSQLConstructType.SUBSELECT
                    || tempStack.peek().getGeneralType() == SUSQLConstructType.UNION) {
                if (isTraceEnabled) {
                    LOG.trace(String.format("Add %s[%s] back to sql construct stack;", tempStack.peek(), tempStack
                            .peek().toSql()));
                }
                sqlConstructStack.push(tempStack.pop());
            } else {
                System.out.println( tempStack.peek().getGeneralType());
                if (isTraceEnabled) {
                    LOG.trace(String.format("Add selected item: %s, %s;", tempStack.peek(), tempStack.peek().toSql()));
                }
                susqlSelect.withSelectedItem(tempStack.pop());
            }
        }
        sql = susqlSelect;
    }

    @Override
    public void enterObjectTypeRef(suqlParser.ObjectTypeRefContext ctx) {
        LOG.trace(">>>> enterObjectTypeRef");
        SUSQLFrom susqlFrom = SUSQLBuilder.build().from().withSQLQueryContext(queryContextStack.peek());
        sqlConstructStack.push(susqlFrom);
    }

    @Override
    public void exitObjectTypeRef(suqlParser.ObjectTypeRefContext ctx) {
        LOG.trace(">>>> exitObjectTypeRef");
        SUSQLTable susqlTable = SUSQLBuilder.build().table().withSQLQueryContext(queryContextStack.peek());
        susqlTable.withTableName(ctx.getText());
        sqlConstructStack.peek().addSubRSC(susqlTable);
    }




    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterFieldExpr(suqlParser.FieldExprContext ctx) {

    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFieldExpr(suqlParser.FieldExprContext ctx) {
        LOG.trace(">>>> exitFieldExpr");
        SUSQLColumn column = SUSQLBuilder.build().column().withSQLQueryContext(queryContextStack.peek());
        column.withColumnName(ctx.getText());
        sqlConstructStack.push(column);
    }

    @Override public void enterFuncCall(suqlParser.FuncCallContext ctx) { }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFuncCall(suqlParser.FuncCallContext ctx) { }



    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSelectField(suqlParser.SelectFieldContext ctx) {



    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSelectField(suqlParser.SelectFieldContext ctx) {
        LOG.trace(">>>> exitSelectField");
        SUSQLCorrelationName susqlCorrelationName = SUSQLBuilder.build().correlationName(ctx.fieldPath().getText());
        /**
         * 暂时不验证数据的属性对不对
         */
        SUSQLColumn susqlConstructCFildExpr = (SUSQLColumn)sqlConstructStack.peek();
        susqlConstructCFildExpr.withAlias(susqlCorrelationName);
    }

}
