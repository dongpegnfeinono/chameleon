package com.shuyun.chameleon.parser;

/**
 * Created by dongpengfei
 * Date 17/9/4
 * Time 上午11:24
 */

public class SUSQLGrammarTokens {
    public enum SetQuantifier {
        DISTINCT, ALL;
    };

    public enum OrderingSpecification {
        ASC, DESC;
    };

    public static final String	FROM			= "FROM";
    public static final String	SELECT			= "SELECT";
    public static final String	WHERE			= "WHERE";
    public static final String	SPACE			= " ";
    public static final String	COMMA			= ",";
    public static final String	DOT				= ".";
    public static final String	AS				= "AS";




}
