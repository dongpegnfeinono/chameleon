// Generated from suql.g4 by ANTLR 4.5.3
package com.shuyun.chameleon.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link suqlParser}.
 */
public interface suqlListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link suqlParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(suqlParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(suqlParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#selectFieldsExpr}.
	 * @param ctx the parse tree
	 */
	void enterSelectFieldsExpr(suqlParser.SelectFieldsExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#selectFieldsExpr}.
	 * @param ctx the parse tree
	 */
	void exitSelectFieldsExpr(suqlParser.SelectFieldsExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#selectSimpleFieldsExpr}.
	 * @param ctx the parse tree
	 */
	void enterSelectSimpleFieldsExpr(suqlParser.SelectSimpleFieldsExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#selectSimpleFieldsExpr}.
	 * @param ctx the parse tree
	 */
	void exitSelectSimpleFieldsExpr(suqlParser.SelectSimpleFieldsExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#selectField}.
	 * @param ctx the parse tree
	 */
	void enterSelectField(suqlParser.SelectFieldContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#selectField}.
	 * @param ctx the parse tree
	 */
	void exitSelectField(suqlParser.SelectFieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#selectPolymorphismFieldsExpr}.
	 * @param ctx the parse tree
	 */
	void enterSelectPolymorphismFieldsExpr(suqlParser.SelectPolymorphismFieldsExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#selectPolymorphismFieldsExpr}.
	 * @param ctx the parse tree
	 */
	void exitSelectPolymorphismFieldsExpr(suqlParser.SelectPolymorphismFieldsExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#subquery}.
	 * @param ctx the parse tree
	 */
	void enterSubquery(suqlParser.SubqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#subquery}.
	 * @param ctx the parse tree
	 */
	void exitSubquery(suqlParser.SubqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#logicalExpr}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpr(suqlParser.LogicalExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#logicalExpr}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpr(suqlParser.LogicalExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#comparisonExpr}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpr(suqlParser.ComparisonExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#comparisonExpr}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpr(suqlParser.ComparisonExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#filterField}.
	 * @param ctx the parse tree
	 */
	void enterFilterField(suqlParser.FilterFieldContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#filterField}.
	 * @param ctx the parse tree
	 */
	void exitFilterField(suqlParser.FilterFieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#groupbyExpression}.
	 * @param ctx the parse tree
	 */
	void enterGroupbyExpression(suqlParser.GroupbyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#groupbyExpression}.
	 * @param ctx the parse tree
	 */
	void exitGroupbyExpression(suqlParser.GroupbyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#aggregationExpr}.
	 * @param ctx the parse tree
	 */
	void enterAggregationExpr(suqlParser.AggregationExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#aggregationExpr}.
	 * @param ctx the parse tree
	 */
	void exitAggregationExpr(suqlParser.AggregationExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#aggregationFuncName}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFuncName(suqlParser.AggregationFuncNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#aggregationFuncName}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFuncName(suqlParser.AggregationFuncNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void enterFuncCall(suqlParser.FuncCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void exitFuncCall(suqlParser.FuncCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#funcName}.
	 * @param ctx the parse tree
	 */
	void enterFuncName(suqlParser.FuncNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#funcName}.
	 * @param ctx the parse tree
	 */
	void exitFuncName(suqlParser.FuncNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#fieldExpr}.
	 * @param ctx the parse tree
	 */
	void enterFieldExpr(suqlParser.FieldExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#fieldExpr}.
	 * @param ctx the parse tree
	 */
	void exitFieldExpr(suqlParser.FieldExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#fieldPath}.
	 * @param ctx the parse tree
	 */
	void enterFieldPath(suqlParser.FieldPathContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#fieldPath}.
	 * @param ctx the parse tree
	 */
	void exitFieldPath(suqlParser.FieldPathContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#fieldName}.
	 * @param ctx the parse tree
	 */
	void enterFieldName(suqlParser.FieldNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#fieldName}.
	 * @param ctx the parse tree
	 */
	void exitFieldName(suqlParser.FieldNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#objectTypeRef}.
	 * @param ctx the parse tree
	 */
	void enterObjectTypeRef(suqlParser.ObjectTypeRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#objectTypeRef}.
	 * @param ctx the parse tree
	 */
	void exitObjectTypeRef(suqlParser.ObjectTypeRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#objectTypeFqn}.
	 * @param ctx the parse tree
	 */
	void enterObjectTypeFqn(suqlParser.ObjectTypeFqnContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#objectTypeFqn}.
	 * @param ctx the parse tree
	 */
	void exitObjectTypeFqn(suqlParser.ObjectTypeFqnContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#objectRealmName}.
	 * @param ctx the parse tree
	 */
	void enterObjectRealmName(suqlParser.ObjectRealmNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#objectRealmName}.
	 * @param ctx the parse tree
	 */
	void exitObjectRealmName(suqlParser.ObjectRealmNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#realm}.
	 * @param ctx the parse tree
	 */
	void enterRealm(suqlParser.RealmContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#realm}.
	 * @param ctx the parse tree
	 */
	void exitRealm(suqlParser.RealmContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#objectTypeName}.
	 * @param ctx the parse tree
	 */
	void enterObjectTypeName(suqlParser.ObjectTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#objectTypeName}.
	 * @param ctx the parse tree
	 */
	void exitObjectTypeName(suqlParser.ObjectTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#valueArrayExpr}.
	 * @param ctx the parse tree
	 */
	void enterValueArrayExpr(suqlParser.ValueArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#valueArrayExpr}.
	 * @param ctx the parse tree
	 */
	void exitValueArrayExpr(suqlParser.ValueArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#valueExpr}.
	 * @param ctx the parse tree
	 */
	void enterValueExpr(suqlParser.ValueExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#valueExpr}.
	 * @param ctx the parse tree
	 */
	void exitValueExpr(suqlParser.ValueExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(suqlParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(suqlParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#scaleType}.
	 * @param ctx the parse tree
	 */
	void enterScaleType(suqlParser.ScaleTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#scaleType}.
	 * @param ctx the parse tree
	 */
	void exitScaleType(suqlParser.ScaleTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#complexType}.
	 * @param ctx the parse tree
	 */
	void enterComplexType(suqlParser.ComplexTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#complexType}.
	 * @param ctx the parse tree
	 */
	void exitComplexType(suqlParser.ComplexTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link suqlParser#timeRangeLiteral}.
	 * @param ctx the parse tree
	 */
	void enterTimeRangeLiteral(suqlParser.TimeRangeLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link suqlParser#timeRangeLiteral}.
	 * @param ctx the parse tree
	 */
	void exitTimeRangeLiteral(suqlParser.TimeRangeLiteralContext ctx);
}