package com.shuyun.chameleon.sql;

/**
 * Created by dongpengfei
 * Date 17/9/4
 * Time 下午4:00
 */

public class SUSQLCorrelationName  extends AbstractSUSQLConstruct {

    private String	correlationName;

    public SUSQLCorrelationName(final String correlationName) {
        this.setCorrelationName(correlationName);
    }

    public void setCorrelationName(final String correlationName) {
        this.correlationName = correlationName;
    }

    public String getName() {
        return this.correlationName;
    }

    @Override
    public String toSql() {
        return correlationName;
    }

    @Override
    public SUSQLConstructType getGeneralType() {
        return SUSQLConstructType.CORRELATION_NAME;
    }

    @Override
    public SUSQLConstructType getActualType() {
        return this.getGeneralType();
    }


    @Override
    public SUSQLConstruct withSQLQueryContext(SUSQLQueryContext sqlQueryContext) {
        return super.withSQLQueryContext(sqlQueryContext);
    }


    @Override
    public SUSQLCorrelationName withId(int identifier) {
        this.identifier = this.getActualType() + "_" + identifier;
        return this;
    }
}
