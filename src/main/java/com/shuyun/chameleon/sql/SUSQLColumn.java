package com.shuyun.chameleon.sql;

import com.shuyun.chameleon.parser.SUSQLGrammarTokens;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 下午5:35
 */

public class SUSQLColumn extends AbstractSUSQLConstruct {

    private String	columnName;
    private int		tableCorrelationNameIndex	= -1;
    private int		columnAliasIndex			= -1;

    public SUSQLColumn() {
    }

    public SUSQLColumn(final String columnName) {
        withColumnName(columnName);
    }

    public SUSQLColumn withColumnName(final String columnName) {
        this.columnName = columnName;
        return this;
    }

    public SUSQLColumn withCorrelationName(final SUSQLCorrelationName tableCorrelationName) {
        if (null != tableCorrelationName) {
            tableCorrelationNameIndex = this.getRepository().addSQLConstruct(tableCorrelationName);
            tableCorrelationName.setReferencingConstruct(this);
        }
        return this;
    }

    public SUSQLColumn withAlias(final SUSQLCorrelationName columnAlias) {
        if (null != columnAlias) {
            this.columnAliasIndex = this.getRepository().addSQLConstruct(columnAlias);
            columnAlias.setReferencingConstruct(this);
        }
        return this;
    }


    @Override
    public SUSQLConstructType getGeneralType() {
        return SUSQLConstructType.COLUMN;
    }

    @Override
    public SUSQLColumn withId(final int identifier) {
        this.identifier = this.getActualType() + "_" + identifier;
        return this;
    }

    @Override
    public String toSql() {
        StringBuilder sql = new StringBuilder();

        if (-1 != tableCorrelationNameIndex) {
            sql.append(this.getRepository().getSQLConstruct(tableCorrelationNameIndex).toSql()).append(SUSQLGrammarTokens.DOT);
        }

        sql.append(columnName);

        if (-1 != columnAliasIndex) {
            sql.append(SUSQLGrammarTokens.SPACE).append(SUSQLGrammarTokens.AS).append(SUSQLGrammarTokens.SPACE)
                    .append(this.getRepository().getSQLConstruct(columnAliasIndex).toSql());
        }

        return sql.toString();
    }

    @Override
    public SUSQLColumn withSQLQueryContext(final SUSQLQueryContext sqlQueryContext) {
        this.sqlQueryContext = sqlQueryContext;
        return this;
    }

    public String getColumnName() {
        return columnName;
    }

    public SUSQLCorrelationName getTableCorrelationName() {
        return (SUSQLCorrelationName) getRepository().getSQLConstruct(tableCorrelationNameIndex);
    }

    public SUSQLCorrelationName getColumnAlias() {
        return (SUSQLCorrelationName) getRepository().getSQLConstruct(columnAliasIndex);
    }
}
