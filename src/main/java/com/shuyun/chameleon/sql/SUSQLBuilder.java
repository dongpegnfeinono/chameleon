package com.shuyun.chameleon.sql;

import org.apache.log4j.Logger;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 下午5:27
 */

public class SUSQLBuilder {

    private static Logger               LOG             = Logger.getLogger(SUSQLBuilder.class);
    private final static SUSQLBuilder	rsc				= new SUSQLBuilder();

    private SUSQLBuilder() {

    }

    public static SUSQLBuilder build() {
        return rsc;
    }

    public SUSQLColumn column() {
        SUSQLColumn column = new SUSQLColumn().withId(SUSQLConstructIdentifierManager.getIdentifier());
        return column;
    }

    public SUSQLCorrelationName correlationName(final String correlationName) {
        SUSQLCorrelationName rscCorrelationname = new SUSQLCorrelationName(correlationName)
                .withId(SUSQLConstructIdentifierManager.getIdentifier());
        return rscCorrelationname;
    }

    public SUSQLFrom from() {
        SUSQLFrom from = new SUSQLFrom().withId(SUSQLConstructIdentifierManager.getIdentifier());
        return from;
    }

    public SUSQLTable table() {
        SUSQLTable table = new SUSQLTable().withId(SUSQLConstructIdentifierManager.getIdentifier());
        return table;
    }

    public SUSQLSelect select() {
        SUSQLSelect select = new SUSQLSelect().withId(SUSQLConstructIdentifierManager.getIdentifier());
        return select;
    }


}
