package com.shuyun.chameleon.sql;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 下午5:33
 */

public enum SUSQLConstructType {
    UNKNOWN,
    SELECT,
    SUBSELECT,
    CORRELATION_NAME,
    FROM,
    TABLE,
    TARGET_RESOURCE,
    WHERE,
    HAVING,
    GROUP_BY,
    ORDER_BY,
    ASTERISK,
    UNION,
    TOKENS, IGNORE_TOKEN, NOT_TOKEN, SIGN_TOKEN,
    COLUMN
}
