package com.shuyun.chameleon.sql;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 下午4:25
 */

public interface SUSQL {

    /**
     * Return the identifier of this RSC construct
     * @return
     */
    String getIdentifier();

    SUSQLConstructType getGeneralType();


    /**
     * This method is called when initialise a new instance of the RSC construct
     * @param identifier
     * @return
     */
    SUSQLConstruct withId(int identifier);

    SUSQLConstructType getActualType();

    String toSql();

    SUSQLConstruct withIndex(int index);

    public SUSQLConstructRepository getRepository();

    void addSubRSC(SUSQLConstruct subRSC);

    SUSQLQueryContext getSQLQueryContext();

    SUSQLConstruct withSQLQueryContext(SUSQLQueryContext sqlQueryContext);

    void setReferencingConstruct(SUSQLConstruct rsc);

    boolean isValidSubRSC(SUSQLConstruct subRSC);


}
