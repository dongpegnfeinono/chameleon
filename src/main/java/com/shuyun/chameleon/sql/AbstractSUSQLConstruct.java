package com.shuyun.chameleon.sql;

import com.shuyun.chameleon.parser.SUSQLGrammarTokens;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 下午4:28
 */

public abstract class AbstractSUSQLConstruct implements SUSQLConstruct {

    private static Logger                       LOG				= Logger.getLogger(AbstractSUSQLConstruct.class);
    protected String				            identifier;
    protected List<SUSQLConstruct>              subRSCList		= new ArrayList<SUSQLConstruct>();
    protected SUSQLConstruct	                referencingRSC;
    /**
     * index in the RSCRepository of the directly impacted referencing RSC construct
     */
    private Integer					            index			= -1;

    private final SUSQLConstructRepository		rscRepository	= new SUSQLConstructRepository();

    protected SUSQLQueryContext		            sqlQueryContext	= SUSQLQueryContext.UNKNWON;


    @Override
    public SUSQLConstructType getGeneralType() {
        return SUSQLConstructType.UNKNOWN;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public SUSQLConstructType getActualType() {
        return getGeneralType();
    }

    @Override
    public void setReferencingConstruct(final SUSQLConstruct referencingRSC) {
        this.referencingRSC = referencingRSC;
    }

    @Override
    public String toSql() {
        StringBuilder stringBuilder = new StringBuilder();

        int size = this.subRSCList.size();
        for (int i = 0; i < size; i++) {
            stringBuilder.append(this.subRSCList.get(i).toSql());
            if (i + 1 != size) {
                stringBuilder.append(SUSQLGrammarTokens.COMMA).append(SUSQLGrammarTokens.SPACE);
            }
        }

        return stringBuilder.toString();
    }

    @Override
    public SUSQLConstruct withIndex(final int index) {
        this.index = index;
        return this;
    }

    @Override
    public boolean isValidSubRSC(final SUSQLConstruct subSQLConstruct) {
        return false;
    }

    @Override
    public void addSubRSC(final SUSQLConstruct subRSC) {
        if (isValidSubRSC(subRSC)) {
            subRSCList.add(subRSC);
            subRSC.setReferencingConstruct(this);
        } else {
            logAddInvalidSubSQLConstruct(LOG, this, subRSC);
        }
    }

    protected void logAddInvalidSubSQLConstruct(final Logger log, final SUSQLConstruct sqlConstruct,
                                                final SUSQLConstruct subSQLConstruct) {
        log.error("trying to add invalid sql construct [" + subSQLConstruct.getGeneralType() + " {"
                + subSQLConstruct.toSql() + "}" + "] to [" + this.getActualType() + "] construct");
    }

    @Override
    public SUSQLConstructRepository getRepository() {
        return this.rscRepository;
    }

    @Override
    public SUSQLQueryContext getSQLQueryContext() {
        return sqlQueryContext;
    }

    @Override
    public SUSQLConstruct withSQLQueryContext(final SUSQLQueryContext sqlQueryContext) {
        this.sqlQueryContext = sqlQueryContext;
        return this;
    }
}
