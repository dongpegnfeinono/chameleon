package com.shuyun.chameleon.sql;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dongpengfei
 * Date 17/9/4
 * Time 下午4:37
 */

public class SUSQLConstructRepository {
    private int											indexer;

    private static Logger LOG									= Logger.getLogger(SUSQLConstructRepository.class);

    private final Map<Integer, SUSQLConstruct> mapOfRSCIdentifierAndRSCConstruct	= new HashMap<Integer, SUSQLConstruct>();

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        String header = String.format("%-30s%-30s%-30s", "Index", "Identifier", "SQL");
        stringBuilder.append(header).append("\n");

        for (Map.Entry<Integer, SUSQLConstruct> entry : mapOfRSCIdentifierAndRSCConstruct.entrySet()) {
            String row = String.format("%-30s%-30s%-30s", entry.getKey(), entry.getValue().getIdentifier(), entry
                    .getValue().toSql());
            stringBuilder.append(row).append("\n");
        }
        return stringBuilder.toString();
    }

    public Integer addSQLConstruct(final SUSQLConstruct rsc) {
        this.mapOfRSCIdentifierAndRSCConstruct.put(++indexer, rsc);
        rsc.withIndex(indexer);
        return indexer;
    }

    public void removeRSC(final int index) {
        this.mapOfRSCIdentifierAndRSCConstruct.remove(index);
    }

    public SUSQLConstruct getSQLConstruct(final int index) {
        return this.mapOfRSCIdentifierAndRSCConstruct.get(index);
    }

    public void replace(final int index, final SUSQLConstruct rsc) {
        LOG.trace("replacing [" + this.mapOfRSCIdentifierAndRSCConstruct.get(index) + "] with [" + rsc + "]");
        this.mapOfRSCIdentifierAndRSCConstruct.put(index, rsc);
        rsc.withIndex(index);
    }
}
