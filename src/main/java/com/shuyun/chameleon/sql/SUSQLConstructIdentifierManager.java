package com.shuyun.chameleon.sql;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by dongpengfei
 * Date 17/9/1
 * Time 下午5:58
 */

public class SUSQLConstructIdentifierManager {
    private static final AtomicInteger seq	= new AtomicInteger();

    public static synchronized void reset() {
        seq.set(0);
    }

    public static synchronized int getIdentifier() {
        return seq.getAndIncrement();
    }

    public static synchronized String getRSCCloneIdentifier(final SUSQLConstruct rsc) {
        return "o_" + rsc.getIdentifier() + "_c_" + seq.getAndIncrement();
    }
}
