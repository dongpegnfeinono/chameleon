package com.shuyun.chameleon.sql;

import com.shuyun.chameleon.parser.SUSQLGrammarTokens;

/**
 * Created by dongpengfei
 * Date 17/9/4
 * Time 下午7:22
 */

public class SUSQLTable extends AbstractSUSQLConstruct {

    private String				tableName;
    private int					correlationNameIndex	= -1;


    private String				schemaName;

    public SUSQLTable() {
    }


    @Override
    public SUSQLConstructType getGeneralType() {
        return SUSQLConstructType.TARGET_RESOURCE;
    }

    @Override
    public SUSQLConstructType getActualType() {
        return SUSQLConstructType.TABLE;
    }

    @Override
    public String toSql() {
        StringBuilder sql = new StringBuilder();

//        if (null != this.schemaName) {
//            sql.append(this.schemaName).append(SUSQLGrammarTokens.DOT);
//        }

        sql.append(SUSQLGrammarTokens.SPACE).append(tableName);

//        if (null != getTableCorrelation()) {
//            sql.append(SQLGrammarTokens.SPACE);
//            sql.append(SQLGrammarTokens.AS).append(SQLGrammarTokens.SPACE);
//            sql.append(getTableCorrelation().toSql());
//        }

//        int numOfJoins = getJoins().size();
//        if (numOfJoins > 0) {
//            for (SQLJoin join : getJoins()) {
//                sql.append(SQLGrammarTokens.SPACE);
//                sql.append(join.toSql());
//            }
//        }

        return sql.toString();
    }

    @Override
    public SUSQLTable withId(int identifier) {
        this.identifier = this.getActualType() + "_" + identifier;
        return this;
    }

    @Override
    public SUSQLTable withSQLQueryContext(final SUSQLQueryContext sqlQueryContext) {
        this.sqlQueryContext = sqlQueryContext;
        return this;
    }

    public SUSQLTable withTableName(final String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getTableName() {
        return tableName;
    }
}
