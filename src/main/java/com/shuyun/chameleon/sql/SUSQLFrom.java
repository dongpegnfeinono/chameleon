package com.shuyun.chameleon.sql;

import com.shuyun.chameleon.parser.SUSQLGrammarTokens;

/**
 * Created by dongpengfei
 * Date 17/9/4
 * Time 下午7:11
 */

public class SUSQLFrom extends AbstractSUSQLConstruct {

    @Override
    public boolean isValidSubRSC(final SUSQLConstruct subSQLConstruct) {
        if (subSQLConstruct.getGeneralType() == SUSQLConstructType.TARGET_RESOURCE) {
            return true;
        }

        return false;
    }

    @Override
    public SUSQLConstructType getGeneralType() {
        return SUSQLConstructType.FROM;
    }

    @Override
    public String toSql() {
        StringBuilder sql = new StringBuilder();

        sql.append(SUSQLGrammarTokens.FROM).append(SUSQLGrammarTokens.SPACE);

        sql.append(super.toSql());

        return sql.toString();
    }

    public SUSQLFrom withSource(final SUSQLConstruct sourceRSC) {
        this.addSubRSC(sourceRSC);
        return this;
    }

    @Override
    public SUSQLFrom withId(final int identifier) {
        this.identifier = this.getActualType() + "_" + identifier;
        return this;
    }

    @Override
    public SUSQLFrom withSQLQueryContext(final SUSQLQueryContext sqlQueryContext) {
        this.sqlQueryContext = sqlQueryContext;
        return this;
    }
}
