package com.shuyun.chameleon.sql;

import com.shuyun.chameleon.parser.SUSQLGrammarTokens;
import com.shuyun.chameleon.parser.SUSQLGrammarTokens.SetQuantifier;

/**
 * Created by dongpengfei
 * Date 17/9/4
 * Time 上午11:26
 */

public class SUSQLSelect extends AbstractSUSQLConstruct {

    protected SetQuantifier setQuantifier;

    private int				whereIndex		= -1;
    private int				fromIndex		= -1;
    private int				havingIndex		= -1;
    private int				groupByIndex	= -1;
    private int				orderByIndex	= -1;

    public SUSQLSelect() {

    }

    @Override
    public SUSQLSelect withId(final int identifier) {
        this.identifier = this.getActualType() + "_" + identifier;
        return this;
    }

    public SUSQLSelect selectAll() {
        this.setQuantifier = SetQuantifier.ALL;
        return this;
    }

    public SUSQLSelect selectDistinct() {
        this.setQuantifier = SetQuantifier.DISTINCT;
        return this;
    }

//    public SUSQLSelect withWhereClause(final SQLWhere where) {
//        this.whereIndex = this.getRepository().addSQLConstruct(where);
//        where.setReferencingConstruct(this);
//        return this;
//    }

    public SUSQLSelect withFromClause(final SUSQLFrom from) {
        this.fromIndex = this.getRepository().addSQLConstruct(from);
        from.setReferencingConstruct(this);
        return this;
    }

    public SUSQLSelect withSelectedItem(final SUSQLConstruct selectedItem) {
        this.addSubRSC(selectedItem);
        return this;
    }

//    public SQLSelect withHavingClause(final SQLHaving having) {
//        this.havingIndex = this.getRepository().addSQLConstruct(having);
//        having.setReferencingConstruct(this);
//        return this;
//    }
//
//    public SQLSelect withGroupBy(final SQLGroupBy groupBy) {
//        this.groupByIndex = this.getRepository().addSQLConstruct(groupBy);
//        groupBy.setReferencingConstruct(this);
//        return this;
//    }
//
//    public SQLSelect withOrderByClause(final SQLOrderBy orderBy) {
//        this.orderByIndex = this.getRepository().addSQLConstruct(orderBy);
//        orderBy.setReferencingConstruct(this);
//        return this;
//    }

//    public SQLWhere getWhere() {
//        return (SQLWhere) this.getRepository().getSQLConstruct(whereIndex);
//    }

    public SUSQLFrom getFrom() {
        return (SUSQLFrom) this.getRepository().getSQLConstruct(fromIndex);
    }

//    public SQLHaving getHaving() {
//        return (SQLHaving) this.getRepository().getSQLConstruct(havingIndex);
//    }
//
//    public SQLGroupBy getGroupBy() {
//        return (SQLGroupBy) this.getRepository().getSQLConstruct(groupByIndex);
//    }
//
//    public SQLOrderBy getOrderBy() {
//        return (SQLOrderBy) this.getRepository().getSQLConstruct(orderByIndex);
//    }

    @Override
    public boolean isValidSubRSC(final SUSQLConstruct rsc) {
        if (rsc.getGeneralType() == SUSQLConstructType.COLUMN) {
            return true;
        }
//      else if (rsc.getGeneralType() == SUSQLConstructType.FUNCTION) {
//            return true;
//        } else if (rsc.getActualType() == SUSQLConstructType.SUBSELECT) {
//            return true;
//        } else if (rsc.getGeneralType() == SUSQLConstructType.SELECTED_ITEM) {
//            return true;
//        } else if (rsc.getGeneralType() == SUSQLConstructType.VALUE) {
//            return true;
//        } else if (rsc.getGeneralType() == SUSQLConstructType.EXPRESSION) {
//            return true;
//        }

        return false;
    }

    @Override
    public SUSQLConstructType getGeneralType() {
        return SUSQLConstructType.SELECT;
    }

    @Override
    public SUSQLConstructType getActualType() {
        return this.getGeneralType();
    }

    public void setSetQuantifier(final SetQuantifier setQuantifier) {
        this.setQuantifier = setQuantifier;
    }

    public SetQuantifier getSetQuantifier() {
        return this.setQuantifier;
    }

    @Override
    public String toSql() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(SUSQLGrammarTokens.SELECT).append(SUSQLGrammarTokens.SPACE);

        if (null != setQuantifier) {
            stringBuilder.append(this.getSetQuantifier()).append(SUSQLGrammarTokens.SPACE);
        }

        stringBuilder.append(super.toSql());

        if (-1 != fromIndex) {
            stringBuilder.append(SUSQLGrammarTokens.SPACE);
            stringBuilder.append(getFrom().toSql());
        }
//        if (-1 != whereIndex) {
//            stringBuilder.append(SUSQLGrammarTokens.SPACE);
//            stringBuilder.append(getWhere().toSql());
//        }
//        if (-1 != groupByIndex) {
//            stringBuilder.append(SQLGrammarTokens.SPACE);
//            stringBuilder.append(getGroupBy().toSql());
//        }
//        if (-1 != havingIndex) {
//            stringBuilder.append(SQLGrammarTokens.SPACE);
//            stringBuilder.append(getHaving().toSql());
//        }
//        if (-1 != orderByIndex) {
//            stringBuilder.append(SQLGrammarTokens.SPACE);
//            stringBuilder.append(getOrderBy().toSql());
//        }

        return stringBuilder.toString();
    }

    @Override
    public SUSQLSelect withSQLQueryContext(final SUSQLQueryContext sqlQueryContext) {
        this.sqlQueryContext = sqlQueryContext;
        return this;
    }
}
